package ru.profi1c.engine.exchange;

import ru.profi1c.engine.app.FbaActivityDialog;
import ru.profi1c.engine.app.FbaApplication;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;

/**
 * Простой наблюдатель за процедурой обмена. Отображает уведомления с установкой
 * действия по клику: <br>
 * При получении новой версии программы — нет действия по клику на уведомлении; <br>
 * При ошибке — по клику отображает диалог с ошибкой; <br>
 * При успешном обмене — по клику откроется главная активность (Activity)
 * программы. <br>
 * <br>
 * Звуки для уведомлений устанавливаются на основании настроек из
 * FbaApplication.getExchangeSettings().
 * 
 */
public class SimpleExchangeObserver extends NotificationExchangeObserver {

	private final FbaApplication app;
	private final BaseExchangeSettings exSettings;
	private String errorMsg;

	public SimpleExchangeObserver(Context context, Handler handler) {
		super(context, handler);
		app = (FbaApplication) context.getApplicationContext();
		exSettings = app.getExchangeSettings();
	}

	@Override
	public Uri getSoungDownloadedApk() {
		return exSettings.getSoundDownloadedApk();
	}

	@Override
	public Uri getSoungExchangeError() {
		return exSettings.getSoundExchangeError();
	}

	@Override
	public Uri getSoungExchangeSuccess() {
		return exSettings.getSoundExchangeSuccess();
	}

	@Override
	public Intent getIntentOnSuccess() {
		return app.getHomeIntent();
	}

	@Override
	public Intent getIntentOnError() {
		return FbaActivityDialog.getStartIntent(getContext(), errorMsg);
	}

	@Override
	public void onStart(ExchangeVariant variant) {

	}

	@Override
	public void onBuild() {

	}

	@Override
	public void onStepInfo(String msg) {

	}

	@Override
	public void onError(String msg) {
		errorMsg = msg;
	}

}
