package ru.profi1c.engine.exchange;

import java.io.File;

import ru.profi1c.engine.util.NotificationHelper;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;

/**
 * Простой интерфейс для наблюдением за изменениями связанными с процедурой
 * обмена. По окончании обмена или при ошибке отображает уведомление
 * 
 */
public abstract class NotificationExchangeObserver extends ExchangeObserver {

	/**
	 * Идентификатор уведомления, которое будет создано при получении новой
	 * версии приложение
	 */
	public static int NOTIFICATION_ID_DOWONLOAD_APK = 7227;

	/**
	 * Идентификатор уведомления, которое будет создано по окончании обмена
	 */
	public static int NOTIFICATION_ID_FINISH_EXCHANGE = 7228;

	/**
	 * Возвращает Uri на звук который будет воспроизведен при получении новой
	 * версии приложения
	 * 
	 * @return
	 */
	public abstract Uri getSoungDownloadedApk();

	/**
	 * Возвращает uri на звук который будет воспроизведен если при обмене
	 * произошли ошибки
	 * 
	 * @return
	 */
	public abstract Uri getSoungExchangeError();

	/**
	 * Возвращает uri на звук который будет воспроизведен если обмен завершен
	 * успешно
	 * 
	 * @return
	 */
	public abstract Uri getSoungExchangeSuccess();

	/**
	 * Возвращает намерение, которое будет запущено по клику на уведомлении об
	 * успешности загрузки
	 * 
	 * @return
	 */
	public abstract Intent getIntentOnSuccess();

	/**
	 * Возвращает намерение, которое будет запущено по клику на уведомлении об
	 * ошибке загрузки
	 * 
	 * @return
	 */
	public abstract Intent getIntentOnError();

	public NotificationExchangeObserver(Context context, Handler handler) {
		super(context, handler);
	}

	@Override
	public boolean onDownloadNewVersionApp(File fApk) {
		NotificationHelper.showDownloadNewVersion(getContext(),
				NOTIFICATION_ID_DOWONLOAD_APK, getSoungDownloadedApk());
		return true;
	}

	@Override
	public void onFinish(boolean success) {
		if (success)
			NotificationHelper.showExchangeSuccess(getContext(),
					NOTIFICATION_ID_FINISH_EXCHANGE, getIntentOnSuccess(),
					getSoungExchangeSuccess());
		else
			NotificationHelper.showExchangeError(getContext(),
					NOTIFICATION_ID_FINISH_EXCHANGE, getIntentOnError(),
					getSoungExchangeError());

	}

}
