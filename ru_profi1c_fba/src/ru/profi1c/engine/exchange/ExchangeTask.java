package ru.profi1c.engine.exchange;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import ru.profi1c.engine.Dbg;
import ru.profi1c.engine.R;
import ru.profi1c.engine.app.ApkUpdateHelper;
import ru.profi1c.engine.meta.DBOpenHelper;
import ru.profi1c.engine.meta.MetadataHelper;
import ru.profi1c.engine.meta.Row;
import ru.profi1c.engine.meta.RowDao;
import ru.profi1c.engine.util.CustomExceptionHandler;
import ru.profi1c.engine.util.FilesHelper;
import android.text.TextUtils;

import com.j256.ormlite.dao.BaseDaoImpl;

/**
 * Реализация процедуры обмена по фиксированным правилам. Требуется разрешение
 * "android.permission.WAKE_LOCK"
 *
 */
public class ExchangeTask extends BaseExchangeTask {
	private static final String TAG = "ExchangeTask";

	/*
	 * Измененные объекты, по которым надо очистить флаг модифицированности при
	 * успешной передаче на сервер
	 */
	private HashMap<Class<?>, List<Object>> mapSendObject;

	public ExchangeTask(ExchangeVariant exchangeVariant, WSHelper wsrvHelper,
			DBOpenHelper dbOpenHelper) {
		super(exchangeVariant, wsrvHelper, dbOpenHelper);
	}

	private File getTmpDir(String subDir) {
		File fDir = new File(appSettings.getCacheDir(), subDir);
		return fDir;
	}

	@Override
	protected boolean doExecute() throws Exception {

		// 1. авторизация
		onStepInfo(R.string.fba_user_autorization);
		boolean isLogin = wsHelper.login();
		if (!isLogin) {
			onError("login", getString(R.string.fba_user_autorization_error));
			return false;
		}

		// 2. отправить на сервер лог ошибок программы
		doSendTraceLog();

		// 3. проверить и скачать новую версию программы
		doCheckNewVersionApp();

		// 4. принудительно очистить данные (если начальная инициализация)
		doClearDBTbales();

		// 5. измененные объекты передать на сервер
		doWriteData();

		// 6. получить данные сервера (новые или измененные)
		HashMap<Class<?>, File> mapJsonData = doGetData();

		if (mapJsonData.size() > 0) {
			// 6.1 уведомить о получении
			doNotifyDataReceipt();

			// 6.2 парсинг json и обновление локальной базы данных
			doUpdateLocalDB(mapJsonData);
		}

		return true;
	}

	/**
	 * Проверить и скачать новую версию приложения если требуется
	 *
	 * @throws WebServiceException
	 */
	private void doCheckNewVersionApp() throws WebServiceException {
		Dbg.log(TAG + ".doCheckNewVersionApp");

		if (!continueExecute()) {
			return;
		}

		// только для этих вариантов обмена
		if (variant == ExchangeVariant.FULL || variant == ExchangeVariant.INIT) {

			onStepInfo(R.string.fba_check_app_version);
			boolean workVersion = wsHelper.isWorkingVersionApp();
			if (!workVersion) {

				onStepInfo(R.string.fba_get_app);
				String fPath = ApkUpdateHelper.getNewVersion(appSettings)
						.getAbsolutePath();
				File newApk = wsHelper.getApp(fPath);
				if (newApk != null) {

					onStepInfo(R.string.fba_get_app_success);
					ResponseHandler.downloadNewVersionApp(newApk);
				}

			}
		}

	}

	/**
	 * Отправить на сервер лог ошибок программы. Лог ошибок фиксируется и
	 * отправляется если это указано в настройках
	 *
	 * @throws WebServiceException
	 */
	private void doSendTraceLog() throws WebServiceException {
		Dbg.log(TAG + ".doSendTraceLog");

		if (!continueExecute()) {
			return;
		}

		if (appSettings.customExceptionHandler()) {

			String strTraceLog = CustomExceptionHandler
					.getErrorTraceLog(getContext());
			if (!TextUtils.isEmpty(strTraceLog)) {

				File fTmp = new File(appSettings.getCacheDir(), "trace_log.tmp");
				if (FilesHelper
						.writeToFile(strTraceLog, fTmp.getAbsolutePath())) {

					onStepInfo(R.string.fba_send_trace_log);
					String id = getString(R.string.fba_id_trace_log);
					wsHelper.writeLargeData(id, null, fTmp.getAbsolutePath(),
							null);
				}
				fTmp.delete();

			}
		}
	}

	/**
	 * Проверить и очистить все таблицы базы данных, если требуется
	 * (принудительная очистка выполняется при начальной инициализации).
	 *
	 * @throws SQLException
	 */
	private void doClearDBTbales() throws SQLException {
		Dbg.log(TAG + ".doClearDBTbales");

		if (!continueExecute()) {
			return;
		}

		if (variant == ExchangeVariant.INIT) {

			/*
			 * Базы данных может и не быть, например когда используются только
			 * получение/сохранение кастомных данных и json объектов
			 */
			if (DBOpenHelper.isExistsDataBase(getContext())
					&& metaHelper != null && dbHelper != null) {

				onStepInfo(R.string.fba_clear_local_db);
				dbHelper.clearTables(metaHelper);
			}

		}
	}

	/**
	 * Выбрать измененные на клиенте объекты и передать их на сервер
	 *
	 * @throws SQLException
	 */
	private void doWriteData() throws WebServiceException, SQLException {
		Dbg.log(TAG + ".doWriteData");

		if (!continueExecute()) {
			return;
		}

		// Удалить временные файлы, оставшиеся с предыдущего обмена
		File fDir = getTmpDir("writeData");
		FilesHelper.removeDirectory(fDir);
		fDir.mkdirs();

		/*
		 * только для этих вариантов обмена и если есть помощник для работы с
		 * метаданными. Отсутствие помощника не является ошибкой - возможен
		 * вариант, когда используются только получение/сохранение кастомных
		 * данных и json объектов. Тогда база данных и метаданные не нужны.
		 */
		if (metaHelper != null
				&& (variant == ExchangeVariant.FULL || variant == ExchangeVariant.ONLY_SAVE)) {

			HashMap<Class<?>, File> mapJsonData = getDataChanges(fDir);
			if (mapJsonData.size() > 0) {

				sendDataToServer(mapJsonData);
				doClearModifiedLocalDB();

			}

		}
	}

	/**
	 * Выбрать из локальной базы данных измененные элементы этого класса,
	 * преобразовать в json и сохранить в указанный файл
	 *
	 * @param clazz
	 *            класс данных
	 * @param fTmpDir
	 *            каталог временных файлов
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private HashMap<Class<?>, File> getDataChanges(File fTmpDir)
			throws SQLException {
		Dbg.log(TAG + ".getDataChanges");

		mapSendObject = new HashMap<Class<?>, List<Object>>();
		HashMap<Class<?>, File> mapJsonData = new HashMap<Class<?>, File>();

		List<Class> lstAll = metaHelper.getAllObjectClases();
		for (Class clazz : lstAll) {

			if (!continueExecute()) {
				break;
			}

			if (clazz != null) {

				String metaType = MetadataHelper.getMetadataType(clazz);
				String metaName = MetadataHelper.getMetadataName(clazz);

				String formatMsg = getString(R.string.fba_select_changes);
				if (formatMsg != null) {
					onStepInfo(String.format(formatMsg, metaType, metaName));
				}

				/*
				 * Имя файла и вложения на английском, для 1С не важно имя, а
				 * встроенный zip не умеет работать с UTF-8 в именах вложений
				 */
				String simpleName = clazz.getSimpleName().replaceAll(".java",
						"");
				String fPath = new File(fTmpDir, String.format("%s.json",
						simpleName)).getAbsolutePath();
				File f = selectChangedToJson(clazz, fPath);
				if (f != null)
					mapJsonData.put(clazz, f);
			}
		}

		return mapJsonData;
	}

	/**
	 * Выбрать из локальной базы данных измененные элементы этого класса
	 *
	 * @param classOfT
	 *            класс данных
	 * @param fPath
	 *            путь к файлу в котором сохраняются данные в формате json
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	private <T> File selectChangedToJson(Class<T> classOfT, String fPath)
			throws SQLException {
		Dbg.log(TAG + ".selectChangedToJson");

		File file = null;
		List<T> lst = null;

		/*
		 * Такой вариант компилируется в Eclipse, но не в Android Studio
		RowDao<Row> dao = dbHelper.getDao(classOfT);
		lst = (List<T>) dao.selectChanged();
		*/
		//совместимый вариант
		BaseDaoImpl<T, String> dao = dbHelper.getDao(classOfT);
		lst = (List<T>) ((RowDao<Row>)dao).selectChanged();

		if (lst != null && lst.size() > 0) {

			// сохранить в кеш для для последующей очистки флага
			// модифицированности
			mapSendObject.put(classOfT, (List<Object>) lst);

			JSONProvider json = getJsonProvider();
			file = new File(fPath);

			boolean sendTo1c = true;
			if (listener != null) {
				boolean update = listener.onSerializeTable(classOfT,
						(List<Object>) lst);
				sendTo1c = update && lst.size() > 0;
			}

			if (sendTo1c) {
				if (!json.toJsonArray(lst, file))
					file = null;
			}

		}
		return file;
	}

	/**
	 * Отправить данные на сервер
	 *
	 * @param mapJsonData
	 * @throws WebServiceException
	 */
	private void sendDataToServer(HashMap<Class<?>, File> mapJsonData)
			throws WebServiceException {
		Dbg.log(TAG + ".sendDataToServer");

		if (!continueExecute()) {
			return;
		}

		for (Class<?> clazz : mapJsonData.keySet()) {

			String metaType = MetadataHelper.getMetadataType(clazz);
			String metaName = MetadataHelper.getMetadataName(clazz);

			String formatMsg = getString(R.string.fba_write_data);
			if (formatMsg != null) {
				onStepInfo(String.format(formatMsg, metaType, metaName));
			}
			String fPath = mapJsonData.get(clazz).getAbsolutePath();
			if (!wsHelper.writeData(metaType, metaName, fPath, "")) {

				if (listener != null) {
					listener.onDeniedDataSavedOnServer(clazz,
							mapSendObject.get(clazz));
				}

				// убрать из кеш-а
				mapSendObject.remove(clazz);
			}

		}

	}

	/**
	 * Получить данные с сервера (новые или измененные)
	 *
	 * @throws WebServiceException
	 */
	@SuppressWarnings("rawtypes")
	private HashMap<Class<?>, File> doGetData() throws WebServiceException {
		Dbg.log(TAG + ".doGetData");

		HashMap<Class<?>, File> mapJsonData = new HashMap<Class<?>, File>();

		if (!continueExecute()) {
			return mapJsonData;
		}

		// Удалить временные файлы, оставшиеся с предыдущего обмена
		File fDir = getTmpDir("getData");
		FilesHelper.removeDirectory(fDir);
		fDir.mkdirs();

		/*
		 * Для всех вариантов кроме 'только сохранение' и если есть помощник для
		 * работы с метаданными. Отсутствие помощника не является ошибкой -
		 * возможен вариант, когда используются только получение/сохранение
		 * кастомных данных и json объектов. Тогда база данных и метаданные не
		 * нужны.
		 */
		if (variant != ExchangeVariant.ONLY_SAVE && metaHelper != null) {

			boolean all = (variant == ExchangeVariant.INIT);

			List<Class> lstAll = metaHelper.getAllObjectClases();
			for (Class clazz : lstAll) {
				File f = getDataOfClass(clazz, all, fDir);
				if (f != null)
					mapJsonData.put(clazz, f);

				if (!continueExecute()) {
					break;
				}

			}

		}

		return mapJsonData;
	}

	/**
	 * Получить данные объекта метаданных, результат сохраняется во временном
	 * файле
	 *
	 * @param clazz
	 * @param all
	 *            запрос всех данных
	 * @param fTmpDir
	 *            каталог временных файлов
	 * @return
	 * @throws WebServiceException
	 */
	@SuppressWarnings("rawtypes")
	private File getDataOfClass(Class clazz, boolean all, File fTmpDir)
			throws WebServiceException {

		File fJson = null;

		if (clazz != null) {

			String metaType = MetadataHelper.getMetadataType(clazz);
			String metaName = MetadataHelper.getMetadataName(clazz);

			String formatMsg = getString(R.string.fba_get_data);
			if (formatMsg != null) {
				onStepInfo(String.format(formatMsg, metaType, metaName));
			}

			/*
			 * Имя файла и вложения на английском, для 1С не важно имя, а
			 * встроенный zip не умеет работать с UTF-8 в именах вложений
			 */
			String simpleName = clazz.getSimpleName().replaceAll(".java", "");
			String fPath = new File(fTmpDir, String.format("%s.json",
					simpleName)).getAbsolutePath();

			fJson = wsHelper.getData(all, metaType, metaName, "", fPath);
		}
		return fJson;
	}

	/**
	 * Уведомить сервер об успешности получения данных, в независимости от того
	 * получены данные полностью или частично (и если не было принудительного
	 * прерывания процедуры обмена)
	 *
	 * @throws WebServiceException
	 */
	private void doNotifyDataReceipt() throws WebServiceException {
		Dbg.log(TAG + ".doNotifyDataReceipt");

		if (!continueExecute()) {
			return;
		}
		onStepInfo(R.string.fba_register_data_receipt);
		wsHelper.registerDataReceipt("");
	}

	/**
	 * Парсинг JSON данных и обновление локальной базы
	 *
	 * @param mapJsonData
	 * @throws SQLException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void doUpdateLocalDB(HashMap<Class<?>, File> mapJsonData)
			throws SQLException {
		Dbg.log(TAG + ".doUpdateLocalDB");

		if (!continueExecute()) {
			return;
		}

		// Передать данные подписчику
		if (listener != null) {
			listener.onDeserializeJson(mapJsonData);
		}

		// В подписчике если и удалили, то не все
		if (mapJsonData.size() > 0) {

			onStepInfo(R.string.fba_update_local_db);
			JSONProvider json = getJsonProvider();

			for (Class clazz : mapJsonData.keySet()) {
				File file = mapJsonData.get(clazz);

				String metaType = MetadataHelper.getMetadataType(clazz);
				String metaName = MetadataHelper.getMetadataName(clazz);

				String formatMsg = getString(R.string.fba_update_table);
				if (formatMsg != null) {
					onStepInfo(String.format(formatMsg, metaType, metaName));
				}

				List<Object> lstData = json.fromJsonArray(clazz, file);
				if (lstData != null) {
					boolean saveToDb = true;
					if (listener != null) {
						boolean update = listener.onUpdateTable(clazz, lstData);
						saveToDb = update && lstData.size() > 0;
					}

					if (saveToDb)
						dbCreateOrUpdateCollection(clazz, lstData);
				}

			}
		}

	}

	/**
	 * Очистить флаг модифицированности по успешно переданным объектам.
	 *
	 * @throws SQLException
	 */
	private void doClearModifiedLocalDB() throws SQLException {
		Dbg.log(TAG + ".doClearModifiedLocalDB");

		if (mapSendObject.size() > 0) {
			// Очистка признака модифицированности строго по переданному списку
			// объектов
			dbCrearModifiedObject(mapSendObject, false);
			mapSendObject.clear();
		}
	}

}
