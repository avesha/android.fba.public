package ru.profi1c.engine.exchange;

import java.util.Calendar;

import ru.profi1c.engine.Dbg;
import ru.profi1c.engine.app.FbaApplication;
import ru.profi1c.engine.app.FbaDBIntentService;
import ru.profi1c.engine.util.DateHelper;
import ru.profi1c.engine.util.NetHelper;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

/**
 * Сервис обмена, выполняет обмен с базой 1С через web-сервис по фиксированным
 * правилам. Это сервис должен быть указан в манифесте вашей программы.
 *
 */
public class ExchangeService extends FbaDBIntentService {
	private static final String TAG = "ExchangeService";

	private static final String EXTRA_VARIANT_ORDINAL = "ex_ordinal";
	private static final String EXTRA_FORCE = "ex_force";

	private BaseExchangeSettings exSettings;
	private ExchangeVariant variant;
	private boolean force;
	private BaseExchangeTask task;

	private ExchangeServiceBinder binder = new ExchangeServiceBinder();
	private ExchangeObserver exObserver;

	/**
	 * Получить намерение, используемое для запуска сервиса. Процедура обмена
	 * выполняется по фиксированным правилам определенным в {@link ExchangeTask}
	 *
	 * @param ctx
	 * @param variant
	 *            вариант обмена
	 * @return
	 */
	public static Intent getIntent(Context ctx, ExchangeVariant variant) {
		Intent i = new Intent(ctx, ExchangeService.class);
		i.putExtra(EXTRA_VARIANT_ORDINAL, variant.ordinal());
		return i;
	}

	/**
	 * Запустить сервис обмена по фиксированным правилам определенным в
	 * {@link ExchangeTask}
	 *
	 * @param ctx
	 */
	public static void start(Context ctx, ExchangeVariant variant) {
		ctx.startService(getIntent(ctx, variant));
	}

	/**
	 * Запустить сервис обмена по фиксированным правилам определенным в
	 * {@link ExchangeTask}. Настройки планировщика игнорируются.
	 *
	 * @param ctx
	 */
	public static void startForce(Context ctx, ExchangeVariant variant) {
		Intent i = getIntent(ctx, variant);
		i.putExtra(EXTRA_FORCE, true);
		ctx.startService(i);
	}

	
	/**
	 * Остановить сервис обмена
	 *
	 * @param ctx
	 */
	public static void stop(Context ctx) {
		ctx.stopService(new Intent(ctx, ExchangeService.class));
	}

	public ExchangeService() {
		super(TAG);
	}

	/**
	 * Возвращает наблюдатель установленный или по умолчанию
	 *
	 * @return
	 */
	private ExchangeObserver getCurrentExchangeObserver() {
		if (exObserver == null) {
			exObserver = new SimpleExchangeObserver(this, new Handler());
		}
		return exObserver;
	}

	/**
	 * Установить наблюдателя за работой сервиса. Если не задан, то используется
	 * наблюдатель по умолчанию, который создает уведомления по завершению
	 * обмена или при возникновении ошибки. <br>
	 * Метод доступен при подключении к сервису через связующий класс
	 * {@link ExchangeServiceBinder}
	 *
	 * @param exchangeObserver
	 */
	public void setExchangeObserver(ExchangeObserver exchangeObserver) {
		exObserver = exchangeObserver;
	}

	/**
	 * Установить задачу, которая выполняет обмен с сервером 1С. Если не
	 * установлена, то обмен выполняется по фиксированным правилам определенным
	 * в {@link ExchangeTask} <br>
	 * Метод доступен при подключении к сервису через связующий класс
	 * {@link ExchangeServiceBinder}
	 *
	 * @param task
	 */
	public void setExchangeTask(BaseExchangeTask exchangeTask) {
		task = exchangeTask;
	}

	public void setForce(boolean force) {
		this.force = force;
	}

	/**
	 * Установить вариант обмена для процедуры обмена по фиксированным правилам
	 * определенным в {@link ExchangeTask}). Метод доступен при подключении к
	 * сервису через связующий класс {@link ExchangeServiceBinder}
	 *
	 * @param exchangeVariant
	 */
	public void setExchangeVariant(ExchangeVariant exchangeVariant) {
		variant = exchangeVariant;
	}

	/**
	 * Получить задачу, которая выполняет обмен с сервером 1С. Если не
	 * установлена, то обмен выполняется по фиксированным правилам определенным
	 * в {@link ExchangeTask}
	 *
	 * @param intent
	 *            Намерение с которым запускается этот сервис
	 * @return
	 */
	protected BaseExchangeTask getExchangeTask(Intent intent) {

		BaseExchangeTask runTask;
		if (task != null) {
			runTask = task;

		}else {

			// выполнять обмен по фиксированным правилам
			// получить текущий вариант обмена
			ExchangeVariant variant = getExchangeVariant(intent);
			if (variant == null) {
				throw new IllegalStateException(
						"ExchangeVariant is not specified when starting a service!");
			}

			runTask = exSettings.getDefaultExchangeTask(variant,getHelper());
			if(runTask==null){
				// хелпер для вызова методов web-сервиса
				WSHelper wsHelper = new WSHelper(exSettings);
				runTask = new ExchangeTask(variant, wsHelper, getHelper());
			}

		}
		return runTask;

	}

	/**
	 * Возвращает текущий вариант обмена, который может быть установлен через
	 * передачу значения намерению @see
	 * {@link ExchangeService#getIntent(Context, ExchangeVariant)} или при
	 * подключении к сервису через связующий класс {@link ExchangeServiceBinder}
	 */
	protected ExchangeVariant getExchangeVariant(Intent startIntent) {
		if (variant == null) {
			// в сервису не подключались через Binder, значить вариант передан
			// через намерение
			int ex_ordinal = startIntent.getIntExtra(EXTRA_VARIANT_ORDINAL, 0);
			variant = ExchangeVariant.values()[ex_ordinal];
		}
		return variant;
	}

	/**
	 * Связующий класс для (локального) доступа к сервису
	 *
	 */
	public class ExchangeServiceBinder extends Binder {
		public ExchangeService getService() {
			return ExchangeService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		super.onBind(intent);
		Dbg.log(TAG + ".onBind");
		return binder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Dbg.log(TAG + ".onCreate");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Dbg.log(TAG + ".onHandleIntent");
		if (intent != null) {

			Context ctx = getApplicationContext();

			FbaApplication app = (FbaApplication) getApplication();
			exSettings = app.getExchangeSettings();

			if (NetHelper.isConnected(ctx)) {

				boolean doRun = true;
				if(intent.hasExtra(EXTRA_FORCE)) 
					force = intent.getBooleanExtra(EXTRA_FORCE, false);
				
				//Не принудительный запуск — проверить расписание 
				if (!force && exSettings.isEnableSchedule()) {
					if (!todayIsWorkDay()) {
						doRun = false;
						Dbg.info(TAG + ", not work day.");
					}
				}

				if (doRun)
					handleStart(intent);

			} else {
				Dbg.info(TAG + ", no connect!");
			}

		}
	}

	private boolean todayIsWorkDay() {
		Calendar cal = Calendar.getInstance();
		int weekDay = cal.get(Calendar.DAY_OF_WEEK);
		return exSettings.isExchangeWeekDays(weekDay);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		doEndExchange();
	}

	private void doStartExchange() {
		ResponseHandler.register(getCurrentExchangeObserver());
	}

	private void doEndExchange() {
		ResponseHandler.unregister(exObserver);
	}

	/*
	 * Начать процедуру обмена
	 */
	private void handleStart(Intent intent) {

		try {
			doStartExchange();

			BaseExchangeTask exTask = getExchangeTask(intent);
			exTask.call();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			doEndExchange();
		}

	}

	/**
	 * Создать задание для автоматического запуска обновления по расписанию.
	 * Процедура обмена выполняется по фиксированным правилам определенным в
	 * {@link ExchangeTask}
	 *
	 * @param ctx
	 * @param variant
	 */
	public static final void createScheduleUpdate(Context ctx,
			BaseExchangeSettings exSetting, ExchangeVariant variant) {

		setRepeatingAlarm(ctx,exSetting,variant);

	}

	private static void setRepeatingAlarm(Context ctx,
			BaseExchangeSettings exSetting, ExchangeVariant variant){

		/*
		 * Расписание ставим на запуск каждый день, а совпадает ли день недели
		 * проверим непосредственно при запуске
		 */
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, exSetting.getExchangeTimeH());
		cal.set(Calendar.MINUTE, exSetting.getExchangeTimeM());
		cal.set(Calendar.SECOND, 0);
		
		if(cal.before(Calendar.getInstance()))
			cal.add(Calendar.DAY_OF_YEAR, 1);
		
		long nextAlarm = cal.getTimeInMillis();

		AlarmManager alarmManager = (AlarmManager) ctx
				.getSystemService(Context.ALARM_SERVICE);

		PendingIntent pi = createPendingIntent(ctx, variant);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, nextAlarm,
				AlarmManager.INTERVAL_DAY, pi);
		Dbg.log(TAG + ".setRepeatingAlarm: next alarm = " +  DateHelper.format(cal.getTime()));
	}


	/**
	 * Отменить задание автоматического обмена
	 *
	 * @param ctx
	 */
	public static final void cancelSchedule(Context ctx, ExchangeVariant variant) {
		Dbg.log(TAG + ".cancelSchedule");

		AlarmManager alarmManager = (AlarmManager) ctx
				.getSystemService(Context.ALARM_SERVICE);
		PendingIntent pi = createPendingIntent(ctx, variant);
		alarmManager.cancel(pi);

	}

	/**
	 * Получить 'отложенное намерение' для запуска сервиса из планировщика
	 *
	 * @param ctx
	 * @param variant
	 * @return
	 */
	private static PendingIntent createPendingIntent(Context ctx,
			ExchangeVariant variant) {

		PendingIntent pendingIntent = PendingIntent.getService(ctx, 0,
				ExchangeService.getIntent(ctx, variant), 0);
		return pendingIntent;
	}

}
