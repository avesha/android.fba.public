package ru.profi1c.engine.report;

import java.util.HashMap;
import java.util.Map;

import ru.profi1c.engine.R;
import ru.profi1c.engine.widget.FieldFormatter;
import android.content.Context;

/**
 * Простой HTML-отчет в виде таблицы с двумя колонками, строится по макету RAW
 * ресурса fba_report_simple_map.html. Содержит три секции заголовков
 * (эквивалент HTML h4,h5,h6), секцию таблицы и секцию подвала. Есть возможность
 * задать цвет: фона отчета, теста и заголовков. </p> <strong>Пример:</strong>
 *
 * <pre>
 * {@code
 * MyMapHtmlReport myMapReport = new MyMapHtmlReport();
 * myMapReport.setHeader2("Наличие товаров на складах");
 * myMapReport.setHeader3("На дату: " + DateHelper.formatShortDate(new Date(System.currentTimeMillis())));
 * myMapReport.setTableHeader("Товар", "Количество");
 *
 * Map<Object,Object> mapData = new LinkedHashMap<Object, Object>();
 * mapData.put("Женские босоножки", 2d);
 * mapData.put("Ботинки женские натуральная кожа", 100.00);
 * mapData.put("Ботинки женские демисезонные", 123.45);
 * mapData.put("-------------------", "-------");
 * mapData.put("Комбайн кухонный BINATONE FP 67", 0d);
 * mapData.put("Кофеварка BRAUN KF22R", 3d);
 *
 * myMapReport.setTableData(mapData);
 * myMapReport.show(this);
 *
 * и класс отчета:
 * private static class MyMapHtmlReport extends SimpleMapHtmlReport{
 *
 * 	public int getResIdIcon() {
 * 		return R.drawable.report_2;
 * 	}
 *
 * 	public int getResIdTitle() {
 * 		return R.string.my_html_map_report;
 * 	}
 *
 * }
 * </pre>
 */
public abstract class SimpleMapHtmlReport extends SimpleReport implements
		IReportBuilder {

	private static final String TABLE_ROW_TEMPLATE = "<tr><td class=\"value\"><!--table_value_1--></td>"
			+ "<td class=\"value\"><!--table_value_2--></td></tr>";

	private HashMap<String, Object> mParams;
	private Map<Object, Object> mTableData;
	private FieldFormatter mFormatter;

	public SimpleMapHtmlReport() {
		mParams = new HashMap<String, Object>();
		// def colors
		setTableBorderColor(0xCCCCCC);
		setTableHeaderBackgroundColor(0xf8f8f8);
	}

	public void setFieldFormatter(FieldFormatter fieldFormatter) {
		this.mFormatter = fieldFormatter;
	}

	public void setReportTitle(String value) {
		mParams.put("report_title", value);
	}

	public void setBackgroundColor(int color) {
		setColorParam("report_background_color", color);
	}

	public void setTextColor(int color) {
		setColorParam("report_text_color", color);
	}

	public void setHeaderTextColor(int color) {
		setColorParam("report_header_text_color", color);
	}

	public void setFooterTextColor(int color) {
		setColorParam("report_footer_text_color", color);
	}

	public void setTableBorderColor(int color) {
		setColorParam("table_border_color", color);
	}

	public void setTableHeaderBackgroundColor(int color) {
		setColorParam("table_header_background_color", color);
	}

	public void setHeader1(String value) {
		mParams.put("header_1", value);
	}

	public void setHeader2(String value) {
		mParams.put("header_2", value);
	}

	public void setHeader3(String value) {
		mParams.put("header_3", value);
	}

	public void setTableHeader(String header1, String header2) {
		mParams.put("table_header_value_1", header1);
		mParams.put("table_header_value_2", header2);
	}

	public void setTableData(Map<Object, Object> data) {
		mTableData = data;
	}

	public void setFooter(String value) {
		mParams.put("footer", value);
	}

	private void setColorParam(String name, int value) {
		String strColor = String.format("#%06X", 0xFFFFFF & value);
		mParams.put(name, strColor);
	}

	@Override
	public IReportBuilder getReportBuilder() {
		return this;
	}

	@Override
	public void build(Context context, IReportBuilderResult builderResult) {

		ReportBundle bundle = ReportBundle.fromRaw(context,
				R.raw.fba_report_simple_map);
		if (bundle != null) {

			bundle.setParams(mParams);

			if (mTableData != null) {
				String tableRows = buildTableRowsSegment();
				bundle.putParam("table_rows", tableRows);
			}

			String data = bundle.build();
			builderResult.onComplete(data);

		}

	}

	private String buildTableRowsSegment() {

		ReportBundle bundle = new ReportBundle(TABLE_ROW_TEMPLATE);
		if (mFormatter != null)
			bundle.setFieldFormatter(mFormatter);

		StringBuilder sb = new StringBuilder();

		for (Object key : mTableData.keySet()) {
			Object value = mTableData.get(key);

			bundle.putParam("table_value_1", key);
			bundle.putParam("table_value_2", value);

			sb.append(bundle.build());
		}
		return sb.toString();
	}

}
