package ru.profi1c.engine.report;

import java.util.Map;

import ru.profi1c.engine.R;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.MailTo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Вывод страницы HTML в диалоге
 *
 */
public class WebViewDialog extends Dialog {

	private static final FrameLayout.LayoutParams FILL;
	static {
		FILL = new FrameLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
	}

	private String mReportData;
	private String mUrl;
	private ProgressDialog mSpinner;
	private ImageView mCrossImage;
	private WebView mWebView;
	private FrameLayout mContent;
	private int mBackgroundColor = 0xCC000000;
	private String mProgressMessage;
	private Map<Object, String> mJavaScriptInterface;

	public WebViewDialog(Context context) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
	}

	public void setReportUrl(String url) {
		mUrl = url;
	}

	public void setReportData(String data) {
		mReportData = data;
	}

	public void setBackgroundColor(int color) {
		mBackgroundColor = color;
	}

	public void setProgressMessage(String message) {
		this.mProgressMessage = message;
	}

	public void setJavaScriptInterfaces(Map<Object, String> mJavaScriptInterface) {
		this.mJavaScriptInterface = mJavaScriptInterface;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// blur/burn in the background
		Window window = getWindow();
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			window.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND,
					WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
		} else {
			window.setBackgroundDrawable(new ColorDrawable(0x70000000));
		}

		mSpinner = new ProgressDialog(getContext());
		mSpinner.requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (mProgressMessage == null)
			mSpinner.setMessage(getContext().getString(
					R.string.fba_report_output));
		else
			mSpinner.setMessage(mProgressMessage);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mContent = new FrameLayout(getContext());

		/*
		 * Create the 'x' image, but don't add to the mContent layout yet at
		 * this point, we only need to know its drawable width and height to
		 * place the webview
		 */
		createCrossImage();

		/*
		 * Now we know 'x' drawable width and height, layout the webivew and add
		 * it the mContent layout
		 */
		int crossWidth = mCrossImage.getDrawable().getIntrinsicWidth();
		setUpWebView(crossWidth / 2);

		/*
		 * Finally add the 'x' image to the mContent layout and add mContent to
		 * the Dialog view
		 */
		FrameLayout.LayoutParams paramPross = new FrameLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		paramPross.gravity = Gravity.TOP | Gravity.LEFT;

		mContent.addView(mCrossImage, paramPross);
		addContentView(mContent, new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
	}

	private void createCrossImage() {
		mCrossImage = new ImageView(getContext());
		// Dismiss the dialog when user click on the 'x'
		mCrossImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				WebViewDialog.this.dismiss();
			}
		});
		Drawable crossDrawable = getContext().getResources().getDrawable(
				R.drawable.fba_dialog_close);
		mCrossImage.setImageDrawable(crossDrawable);
		/*
		 * 'x' should not be visible while webview is loading make it visible
		 * only after webview has fully loaded
		 */
		mCrossImage.setVisibility(View.INVISIBLE);

	}

	@SuppressLint({ "NewApi", "SetJavaScriptEnabled" })
	private void setUpWebView(int margin) {
		LinearLayout webViewContainer = new LinearLayout(getContext());

		mWebView = new WebView(getContext());
		mWebView.setVerticalScrollBarEnabled(false);
		mWebView.setHorizontalScrollBarEnabled(false);
		mWebView.setWebViewClient(new WebViewDialog.ReportWebViewClient());
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.setBackgroundColor(mBackgroundColor);
		mWebView.getSettings().setSupportZoom(true);
		mWebView.getSettings().setBuiltInZoomControls(true);

		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {
			mWebView.getSettings().setDisplayZoomControls(true);
		}

		if (mJavaScriptInterface != null) {
			for (Object obj : mJavaScriptInterface.keySet()) {
				if (obj != null)
					mWebView.addJavascriptInterface(obj,
							mJavaScriptInterface.get(obj));
			}
		}

		if (!TextUtils.isEmpty(mReportData)) {
			mWebView.loadDataWithBaseURL("x-data://base", mReportData,
					"text/html", "UTF-8", null);
		} else if (mUrl != null) {
			mWebView.loadUrl(mUrl);
		} else
			throw new IllegalStateException("Not sets report data or url!");

		mWebView.setLayoutParams(FILL);
		mWebView.setVisibility(View.INVISIBLE);

		webViewContainer.setPadding(margin, margin, margin, margin);
		webViewContainer.addView(mWebView);
		mContent.addView(webViewContainer);
	}

	private class ReportWebViewClient extends WebViewClient {

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			mSpinner.show();
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			mSpinner.dismiss();
			/*
			 * Once webview is fully loaded, set the mContent background to be
			 * transparent and make visible the 'x' image.
			 */
			mContent.setBackgroundColor(Color.TRANSPARENT);
			mWebView.setVisibility(View.VISIBLE);
			mCrossImage.setVisibility(View.VISIBLE);

		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Intent i = null;
			if (url.startsWith("tel:")) {
				i = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
			} else if (url.startsWith("mailto:")) {
				MailTo mt = MailTo.parse(url);
				i = new Intent(Intent.ACTION_SEND);
				i.setType("text/plain");
				i.putExtra(Intent.EXTRA_EMAIL, new String[] { mt.getTo() });
				i.putExtra(Intent.EXTRA_SUBJECT, mt.getSubject());
				i.putExtra(Intent.EXTRA_CC, mt.getCc());
				i.putExtra(Intent.EXTRA_TEXT, mt.getBody());

			} else {
				i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			}

			try {
				getContext().startActivity(i);
			} catch (ActivityNotFoundException e) {
				e.printStackTrace();
			}
			return true;

		}
		
	}


}
