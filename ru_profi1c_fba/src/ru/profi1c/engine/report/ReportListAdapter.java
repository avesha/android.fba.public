package ru.profi1c.engine.report;

import java.util.List;

import ru.profi1c.engine.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Адаптер для списка отчетов
 * 
 */
public class ReportListAdapter extends BaseAdapter {

	private final LayoutInflater li;
	private final List<IReport> items;

	public ReportListAdapter(Context ctx, List<IReport> items) {
		li = LayoutInflater.from(ctx);
		this.items = items;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public IReport getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewHolder holder = null;

		IReport item = getItem(position);

		if (row == null) {
			row = li.inflate(R.layout.fba_simple_report_item, parent, false);

			holder = new ViewHolder(row);
			row.setTag(holder);

		} else {
			holder = (ViewHolder) row.getTag();
		}

		holder.icon.setImageResource(item.getResIdIcon());
		holder.title.setText(item.getResIdTitle());

		return row;

	}

	private static class ViewHolder {

		ImageView icon;
		TextView title;

		public ViewHolder(View base) {
			icon = (ImageView) base.findViewById(android.R.id.icon);
			title = (TextView) base.findViewById(android.R.id.text1);
		}
	}
}
