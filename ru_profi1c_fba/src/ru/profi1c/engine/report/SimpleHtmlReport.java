package ru.profi1c.engine.report;

import java.util.HashMap;

import ru.profi1c.engine.R;
import android.content.Context;

/**
 * Простой HTML – отчет, строится по макету из RAW ресурса
 * fba_report_simple.html.
 * <p>
 * Содержит три секции заголовков (эквивалент HTML h4,h5,h6), секцию данных и
 * секцию подвала. Есть возможность задать цвет: фона отчета, теста и
 * заголовков.
 * </p>
 * <strong>Пример:</strong>
 * 
 * <pre>
 * {@code 
 *  MyHtmlReport1 myReport = new MyHtmlReport1();
 *  myReport.setBackgroundColor(getResources().getColor(android.R.color.black));
 *  myReport.setHeaderTextColor(getResources().getColor(android.R.color.holo_blue_light));
 *  myReport.setTextColor(getResources().getColor(android.R.color.white));
 *  myReport.setHeader1("Это заголовок отчета");
 *  myReport.setText("Это произвольный текст отчета. Допускается простейшее форматирование, например <strong>жирный текст</strong> и т.п");
 *  
 *  и класс отчета:
 *  private static class MyHtmlReport1 extends SimpleHtmlReport{
 *  	
 *  	public int getResIdIcon() {
 *  		return R.drawable.report_1;
 *  	}
 *  
 *  	public int getResIdTitle() {
 *  		return R.string.my_html_report1;
 *  	}
 *  	
 *  }
 * </pre>
 * 
 */
public abstract class SimpleHtmlReport extends SimpleReport implements
		IReportBuilder {

	private HashMap<String, Object> mParams;

	public SimpleHtmlReport() {
		mParams = new HashMap<String, Object>();
	}

	public void setReportTitle(String value) {
		mParams.put("report_title", value);
	}

	public void setBackgroundColor(int color) {
		setColorParam("report_background_color", color);
	}

	public void setTextColor(int color) {
		setColorParam("report_text_color", color);
	}

	public void setHeaderTextColor(int color) {
		setColorParam("report_header_text_color", color);
	}

	public void setFooterTextColor(int color) {
		setColorParam("report_footer_text_color", color);
	}

	public void setHeader1(String value) {
		mParams.put("header_1", value);
	}

	public void setHeader2(String value) {
		mParams.put("header_2", value);
	}

	public void setHeader3(String value) {
		mParams.put("header_3", value);
	}

	public void setText(String value) {
		mParams.put("text", value);
	}

	public void setFooter(String value) {
		mParams.put("footer", value);
	}

	private void setColorParam(String name, int value) {
		String strColor = String.format("#%06X", 0xFFFFFF & value);
		mParams.put(name, strColor);
	}

	@Override
	public IReportBuilder getReportBuilder() {
		return this;
	}

	@Override
	public void build(Context context, IReportBuilderResult builderResult) {

		ReportBundle bundle = ReportBundle.fromRaw(context,
				R.raw.fba_report_simple);
		if (bundle != null) {
			bundle.setParams(mParams);

			String data = bundle.build();
			builderResult.onComplete(data);
		}

	}

}
