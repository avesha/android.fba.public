package ru.profi1c.engine.report;

import java.io.File;

import ru.profi1c.engine.util.FilesHelper;

import android.content.Context;

/**
 * Простой скомпилированный HTML отчет. Источником может выступать внешний файл
 * или assert - ресурс, результат выводится в html – диалог
 * 
 */
public abstract class SimpleCompiledHtmlReport extends SimpleReport {

	private File mFile;
	private String mPathToAsset;

	/**
	 * Создать отчет на основании внешнего файла
	 * 
	 * @param file
	 */
	public SimpleCompiledHtmlReport(File file) {
		mFile = file;
	}

	/**
	 * Создать отчет на основании внутреннего актива приложения
	 * 
	 * @param pathToAsset
	 *            Путь к ресурсу в Assets (исключая сам корневой каталог Assets)
	 */
	public SimpleCompiledHtmlReport(String pathToAsset) {
		mPathToAsset = pathToAsset;
	}

	@Override
	public IReportBuilder getReportBuilder() {
		if (mFile != null)
			return new RawFileReportBuilder(mFile);
		return new AssertReportBuilder();
	}

	private class AssertReportBuilder implements IReportBuilder {

		@Override
		public void build(Context context, IReportBuilderResult builderResult) {
			String data = FilesHelper.getAssetsData(context, mPathToAsset);
			builderResult.onComplete(data);
		}

	}
}
