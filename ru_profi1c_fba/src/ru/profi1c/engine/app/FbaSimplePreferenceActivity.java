package ru.profi1c.engine.app;

import ru.profi1c.engine.R;
import ru.profi1c.engine.exchange.BaseExchangeSettings;
import ru.profi1c.engine.exchange.ExchangeReceiver;
import ru.profi1c.engine.util.AppHelper;
import ru.profi1c.engine.util.DateHelper;
import ru.profi1c.engine.widget.TimePreference;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.res.Resources;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.RingtonePreference;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

/**
 * Простая реализация редактирования предпочтений (настроек программы) .
 * Возможность указать: <br>
 * - имя пользователя и пароль, используемые для входа в программу и для
 * авторизации на web-сервисе 1С; <br>
 * - ip-адрес web-сервиса; <br>
 * - расписание (время и дни недели) для выполнения задания обмена в фоновом
 * режиме; <br>
 * Эта активити должна быть указан в манифесте вашей программы.
 * 
 */
public class FbaSimplePreferenceActivity extends FbaPreferenceActivity {
	private static final String TAG = "FbaSimplePreferenceActivity";

	private BaseExchangeSettings exSettings;

	private TimePreference mExchangeScheduleTime;
	private Preference mExchangeScheduleDays;

	private String[] weekDays, shortWeekDays;
	private int[] indexWeekDays;
	private boolean[] checkedWeekDays;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.fba_simple_preference);

		exSettings = getExchangeSettings();
		init();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		exSettings.save();
	}

	private void init() {

		initBaseExchangeSettings();
		initAbout();
		initExchangeScheduleScreen();
	}

	private void initBaseExchangeSettings() {

		// имя пользователя
		EditTextPreference etPref = (EditTextPreference) findPreference("fba_key_user_name");
		etPref.setText(exSettings.getUserName());
		etPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

			@Override
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
				exSettings.setUserName((String) newValue);
				return true;
			}
		});

		// пароль
		etPref = (EditTextPreference) findPreference("fba_key_user_password");
		etPref.setText(exSettings.getPassword());
		etPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

			@Override
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
				exSettings.setPassword((String) newValue);
				return true;
			}
		});

		// адрес сервера
		etPref = (EditTextPreference) findPreference("fba_key_server_ip");
		etPref.setText(exSettings.getServerIP());
		etPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

			@Override
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
				exSettings.setServerIP((String) newValue);
				return true;
			}
		});

		// серийный номер этого устройства
		Preference pref = findPreference("fba_key_device_id");
		pref.setSummary(exSettings.getDeviceId());

	}

	/**
	 * инициализация данный для расписания автоматической загрузки
	 */
	private void initExchangeScheduleScreen() {

		CheckBoxPreference enableSchedule = (CheckBoxPreference) findPreference("fba_key_enable_exchange_schedule");
		enableSchedule.setChecked(exSettings.isEnableSchedule());
		enableSchedule
				.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

					@Override
					public boolean onPreferenceChange(Preference preference,
							Object newValue) {
						boolean isEnable = newValue.equals(true);
						exSettings.setEnableSchedule(isEnable);
						changeUpdateSchedule();
						return true;
					}
				});

		// установка времени
		mExchangeScheduleTime = (TimePreference) findPreference("fba_key_exchange_schedule_time");
		mExchangeScheduleTime.setLastHour(exSettings.getExchangeTimeH());
		mExchangeScheduleTime.setLastMinute(exSettings.getExchangeTimeM());
		fortamExchangeTimeSummary(exSettings.getExchangeTimeH(),
				exSettings.getExchangeTimeM());

		mExchangeScheduleTime
				.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

					@Override
					public boolean onPreferenceChange(Preference preference,
							Object newValue) {
						String time = (String) newValue;
						int hour = TimePreference.getHour(time);
						int minute = TimePreference.getMinute(time);

						exSettings.setExchangeTimeH(hour);
						exSettings.setExchangeTimeM(minute);

						changeUpdateSchedule();

						fortamExchangeTimeSummary(hour, minute);
						return true;
					}
				});

		// выбор дней
		mExchangeScheduleDays = findPreference("fba_key_exchange_schedule_days");
		mExchangeScheduleDays
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference preference) {
						setExchangeScheduleDays();
						return false;
					}
				});

		weekDays = DateHelper.getWeekdays();
		shortWeekDays = DateHelper.getShortWeekdays();
		indexWeekDays = DateHelper.getIndexWeekdays();
		checkedWeekDays = new boolean[7];

		for (int i = 0; i < weekDays.length; i++)
			checkedWeekDays[i] = exSettings
					.isExchangeWeekDays(indexWeekDays[i]);

		formatSummaryExchangeDays();

		// звуки
		RingtonePreference ringPref = (RingtonePreference) findPreference("fba_key_sound_sucsess");
		String soundUri = null;
		if (exSettings.getSoundExchangeSuccess() != null) {
			soundUri = exSettings.getSoundExchangeSuccess().toString();
			ringPref.setDefaultValue(soundUri);
		}
		formatSoundSummary(ringPref, soundUri);

		ringPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

			@Override
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
				String uri = (String) newValue;

				exSettings.setSoundSucsess(uri);
				formatSoundSummary((RingtonePreference) preference, uri);
				return true;
			}
		});

		ringPref = (RingtonePreference) findPreference("fba_key_sound_error");
		soundUri = null;
		if (exSettings.getSoundExchangeError() != null) {
			soundUri = exSettings.getSoundExchangeError().toString();
			ringPref.setDefaultValue(soundUri);
		}
		formatSoundSummary(ringPref, soundUri);

		ringPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

			@Override
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
				String uri = (String) newValue;

				exSettings.setSoundError(uri);
				formatSoundSummary((RingtonePreference) preference, uri);
				return true;
			}
		});

		ringPref = (RingtonePreference) findPreference("fba_key_sound_new_app");
		soundUri = null;
		if (exSettings.getSoundDownloadedApk() != null) {
			soundUri = exSettings.getSoundDownloadedApk().toString();
			ringPref.setDefaultValue(soundUri);
		}
		formatSoundSummary(ringPref, soundUri);

		ringPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

			@Override
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
				String uri = (String) newValue;

				exSettings.setSoundNewApp(uri);
				formatSoundSummary((RingtonePreference) preference, uri);
				return true;
			}
		});

	}

	protected void fortamExchangeTimeSummary(int hour, int minute) {
		mExchangeScheduleTime.setSummary(TimePreference
				.formatTime(hour, minute));
	}

	/**
	 * Показать сокращенные названия дней недели
	 */
	protected void formatSummaryExchangeDays() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < checkedWeekDays.length; i++) {
			if (checkedWeekDays[i])
				sb.append(shortWeekDays[i]).append(",");
		}
		if (sb.length() > 0)
			sb.setLength(sb.length() - 1);
		mExchangeScheduleDays.setSummary(sb.toString());
	}

	/**
	 * Выбор дней в диалоге
	 */
	protected void setExchangeScheduleDays() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.fba_days_of_week);
		builder.setMultiChoiceItems(weekDays, checkedWeekDays,
				new OnMultiChoiceClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int position,
							boolean isChecked) {
						checkedWeekDays[position] = isChecked;
						exSettings.setExchangeWeekDays(indexWeekDays,
								checkedWeekDays);

						changeUpdateSchedule();

						formatSummaryExchangeDays();

					}
				});
		builder.setPositiveButton(android.R.string.ok, null);
		builder.setNegativeButton(android.R.string.cancel, null);
		builder.create().show();
	}

	/**
	 * В резюме показать наименование мелодии
	 * 
	 * @param ringPref
	 * @param uriString
	 */
	protected void formatSoundSummary(RingtonePreference ringPref,
			String uriString) {
		String summary = getString(R.string.fba_mute);
		if (!TextUtils.isEmpty(uriString)) {
			Uri uri = Uri.parse(uriString);
			Ringtone ringtone = RingtoneManager.getRingtone(this, uri);
			summary = ringtone.getTitle(this);

		}
		ringPref.setSummary(summary);
	}

	/**
	 * about the program
	 */
	private void initAbout() {

		Preference prefAbout = findPreference("fba_key_about");
		prefAbout.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			public boolean onPreferenceClick(Preference preference) {

				final Context ctx = FbaSimplePreferenceActivity.this;

				LayoutInflater li = LayoutInflater.from(ctx);
				View view = li.inflate(R.layout.fba_simple_about_dialog_layout,
						null);

				AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
				builder.setView(view);

				int idResIcon = getAppSettings().getIdResIconLauncher();
				if (idResIcon == 0)
					idResIcon = R.drawable.fba_ic_launcher;

				builder.setIcon(idResIcon);
				builder.setTitle(R.string.app_name);

				Resources res = ctx.getResources();
				final String ab_info = res.getString(R.string.app_descr);

				// set name version of app
				TextView tvVersionName = (TextView) view
						.findViewById(R.id.fba_version_name);
				String versionName = AppHelper.getAppVersion(ctx);

				StringBuilder sb = new StringBuilder();
				sb.append("v ").append(versionName);
				tvVersionName.setText(sb.toString());

				// format the description as html
				TextView tvAboutInfo = (TextView) view
						.findViewById(R.id.fba_about_info);
				tvAboutInfo.setText(Html.fromHtml(ab_info));

				builder.setNegativeButton(R.string.fba_ok, null);

				AlertDialog ad = builder.create();
				ad.setCanceledOnTouchOutside(true);
				ad.show();

				return true;
			}
		});
	}

	/**
	 * Запустить/остановить планировщик обмена согласно настроек.
	 */
	protected void changeUpdateSchedule() {
		ExchangeReceiver.createSchedulerTasks(getApplicationContext());
	}
}
