package ru.profi1c.engine.app;

import ru.profi1c.engine.R;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;

/**
 * Запрос пароля при запуске приложения. Пароль проверяется просто
 * на равенство, если неправильный срабатывает анимация и вибрация, ввод пароля
 * по кнопке 'Далее'
 *
 */
public class FbaSecurityNumericActivity extends FbaActivity {

	private EditText etPassword;
	private String password;

	private Animation errPass;
	private Vibrator vibrator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		/*
		 * getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		 * WindowManager.LayoutParams.FLAG_FULLSCREEN);
		 */

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			getSupportActionBar().hide();
			// выше 3.0 автоматически скрывают ActionBar если установлен флаг
			// Window.FEATURE_NO_TITLE
		}

		setContentView(R.layout.fba_activity_security_numeric_layout);

		password = getExchangeSettings().getPassword();

		etPassword = (EditText) findViewById(R.id.fba_password);
		etPassword.setOnKeyListener(OnKeyParrwordListener);

		EditText etUserName = (EditText) findViewById(R.id.fba_user_name);
		etUserName.setText(getExchangeSettings().getUserName());

		View btn = findViewById(R.id.fba_next);
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				checkPassword();
			}
		});

		errPass = AnimationUtils.loadAnimation(this, R.anim.fba_err_pass);

	}

	private boolean isPassOk() {
		boolean isOk = false;

		String inputPass = etPassword.getText().toString().toLowerCase();
		if (inputPass.equals(password.toLowerCase())) {
			isOk = true;
		}
		return isOk;
	}

	private void checkPassword() {

		if (isPassOk()) {
			result(RESULT_OK);
		} else {
			vibrator.vibrate(200);
			etPassword.startAnimation(errPass);
		}
	}

	private OnKeyListener OnKeyParrwordListener = new OnKeyListener() {

		public boolean onKey(View v, int keyCode, KeyEvent keyevent) {

			if (keyevent.getAction() == KeyEvent.ACTION_DOWN) {
				return false;
			}

			if (keyCode == KeyEvent.KEYCODE_ENTER) {
				checkPassword();
			} else {
				if (isPassOk()) {
					result(RESULT_OK);
				}
			}
			return false;
		}
	};

	private void result(int resultCode) {
		setResult(resultCode);
		finish();
	}

}
