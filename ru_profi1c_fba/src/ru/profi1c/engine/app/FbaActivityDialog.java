package ru.profi1c.engine.app;

import ru.profi1c.engine.Dbg;
import ru.profi1c.engine.R;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Activity внешне выглядящая как диалог. Отображает текст сообщения и кнопку
 * закрытия, при нажатии на которую происходит переход к основной Activity
 * вашего приложения.
 * 
 * 
 */
public class FbaActivityDialog extends FbaActivity {
	private static final String TAG = "FbaActivityDialog";

	private String msg;

	public static Intent getStartIntent(Context ctx, String msg) {
		Intent i = new Intent(ctx, FbaActivityDialog.class);
		// в истории по кнопке "Домой" этой активити не будет
		i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		// установим флаг, что надо новую активити - иначе intent будет
		// от предыдущей при открытии
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		i.putExtra("msg", msg);

		return i;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Dbg.log(TAG + ".onCreate");

		setContentView(R.layout.fba_activity_dialog_layout);
		msg = getIntent().getExtras().getString("msg");
		initControl();
	}

	private void initControl() {

		if (!TextUtils.isEmpty(msg)) {
			TextView description = (TextView) findViewById(R.id.fba_description);
			description.setMovementMethod(new ScrollingMovementMethod());
			description.setText(msg);
		}

		Button btn = (Button) findViewById(R.id.fba_ok);
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				goHome(FbaActivityDialog.this);
				finish();
			}
		});
	}

}
