package ru.profi1c.engine.app;

import ru.profi1c.engine.Dbg;
import ru.profi1c.engine.R;
import ru.profi1c.engine.exchange.BaseExchangeTask;
import ru.profi1c.engine.exchange.ExchangeObserver;
import ru.profi1c.engine.exchange.ExchangeService;
import ru.profi1c.engine.exchange.ExchangeTask;
import ru.profi1c.engine.exchange.ExchangeVariant;
import ru.profi1c.engine.exchange.ResponseHandler;
import ru.profi1c.engine.exchange.SimpleExchangeObserver;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

/**
 * Базовый класс активити с наблюдателем за сервисом обмена
 *
 */
public abstract class FbaDBExchangeActivity extends FbaDBActivity implements
		OnCancelListener {
	private static final String TAG = "FbaDBExchangeActivity";

	private ExchangeService mExchangeService;
	private Intent mIntentService;
	private ServiceConnection mConn;
	private boolean mBound = false;

	private ExchangeObserver mWSExchangeObserver;
	private Handler mExchangeHandler;

	private ProgressDialog mProgressDialog;
	private boolean mCancelableExchange;
	private boolean mCanceled;

	/**
	 * Ваша реализация наблюдателя, если не уставлен — используется внутренний
	 * который просто отображает модальный диалог с отображением прогресса
	 * обмена
	 *
	 * @return
	 */
	protected abstract ExchangeObserver getExchangeObserver();

	/**
	 * Запустить обмен с web-сервисом. Процедура обмена выполняется по
	 * фиксированным правилам определенным в {@link ExchangeTask}
	 *
	 * @param variant
	 *            вариант обмена
	 * @param cancelable
	 *            возможность отмены процедуры обмена
	 */
	public void startExchange(ExchangeVariant variant, boolean cancelable) {
		if (mBound) {
			mCancelableExchange = cancelable;
			mCanceled = false;
			ResponseHandler.register(mWSExchangeObserver);

			startExchangeServiсe(variant);
		} else {
			Dbg.info(TAG + ": Could not connect to the exchange service!");
		}
	}

	/**
	 * Запустить обмен с web-сервисом по вашим правилам
	 *
	 * @param exchangeTask
	 *            задача обмена
	 * @param cancelable
	 *            возможность отмены процедуры обмена
	 */
	public void startExchange(BaseExchangeTask exchangeTask, boolean cancelable) {

		if (mBound) {
			mCancelableExchange = cancelable;
			mCanceled = false;
			ResponseHandler.register(mWSExchangeObserver);

			startExchangeServiсe(exchangeTask);
		} else {
			Dbg.info(TAG + ": Could not connect to the exchange service!");
		}

	}

	/**
	 * Принудительно прервать обмен (не рекомендуется)
	 *
	 * @param force
	 *            если истина, сервис принудительно останавливается сразу, иначе
	 *            останавливается при первой возможности - по завершении
	 *            атомарной операции
	 */
	public void cancelExchange(boolean force) {
		mCanceled = true;
		ResponseHandler.cancel();
		ResponseHandler.unregister(mWSExchangeObserver);
		closeProgressExchangeDlg();

		if (force) {
			if (mBound)
				mExchangeService.stopSelf();
			else
				ExchangeService.stop(getApplicationContext());
		}
	}

	/**
	 * Возвращает наблюдатель установленный или по умолчанию
	 *
	 * @return
	 */
	private ExchangeObserver getCurrentExchangeObserver() {
		ExchangeObserver observer = getExchangeObserver();
		if (observer == null)
			observer = new WSSimpleExchangeObserver(this, mExchangeHandler);
		return observer;
	}

	/**
	 * Запустить сервис обмена
	 *
	 * @param variant
	 *            вариант обмена
	 */
	private void startExchangeServiсe(ExchangeVariant variant) {
		mExchangeService.setExchangeVariant(variant);
		mExchangeService.setExchangeObserver(getCurrentExchangeObserver());
		// При ручном запуске обмена расписание не учитывается.
		mExchangeService.setForce(true);
		startService(mIntentService);
	}

	/**
	 * Запустить сервис обмена
	 *
	 * @param exchangeTask
	 *            задача обмена
	 */
	private void startExchangeServiсe(BaseExchangeTask exchangeTask) {
		mExchangeService.setExchangeTask(exchangeTask);
		mExchangeService.setExchangeObserver(getCurrentExchangeObserver());
		startService(mIntentService);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/*
		 * намерение на запуск, все остальные параметры передаются сервису через
		 * подключение непосредственно перед запуском.
		 */
		mIntentService = new Intent(this, ExchangeService.class);

		// подключение к сервису
		mConn = new ServiceConnection() {

			@Override
			public void onServiceDisconnected(ComponentName name) {
				Dbg.log(TAG + ".onServiceDisconnected: " + name.getClassName());
				mBound = false;
			}

			@Override
			public void onServiceConnected(ComponentName name, IBinder builder) {
				Dbg.log(TAG + ".onServiceConnected: " + name.getClassName());
				mExchangeService = ((ExchangeService.ExchangeServiceBinder) builder)
						.getService();
				mBound = true;
			}
		};

		mExchangeHandler = new Handler();
		mWSExchangeObserver = getCurrentExchangeObserver();
	}

	@Override
	protected void onStart() {
		super.onStart();

		bindService(mIntentService, mConn, Context.BIND_AUTO_CREATE);
		ResponseHandler.register(mWSExchangeObserver);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (mBound) {
			unbindService(mConn);
			mBound = false;
		}
		ResponseHandler.unregister(mWSExchangeObserver);
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		cancelExchange(false);
	}

	/**
	 * Создать диалог — прогрессор для отображения процесса обмена с
	 * web-сервисом
	 */
	protected void createProgressExchangeDlg() {

		mExchangeHandler.post(new Runnable() {

			@Override
			public void run() {

				if (mProgressDialog == null) {
					// Setup progress dialog
					mProgressDialog = new ProgressDialog(
							FbaDBExchangeActivity.this);
					mProgressDialog.setIndeterminate(true);
					mProgressDialog.setCancelable(mCancelableExchange);
					mProgressDialog.setCanceledOnTouchOutside(false);
					mProgressDialog
							.setOnCancelListener(FbaDBExchangeActivity.this);
				}

			}
		});

	}

	protected void onProgress(final String message) {

		mExchangeHandler.post(new Runnable() {

			@Override
			public void run() {

				if (mProgressDialog == null)
					throw new IllegalStateException(
							"You first need to call 'createProgressExchangeDlg' dialog!");

				if (!isFinishing() && !mProgressDialog.isShowing()) {
					mProgressDialog.show();
				}
				// Show current message in progress dialog
				mProgressDialog.setMessage(message);

			}
		});

	}

	/**
	 * Закрыть диалог отображающий процесс обмена с web-сервисом
	 */
	protected void closeProgressExchangeDlg() {

		mExchangeHandler.post(new Runnable() {

			@Override
			public void run() {

				if (mProgressDialog != null) {
					mProgressDialog.dismiss();
					mProgressDialog = null;
				}

			}
		});

	}

	/**
	 * Наблюдатель за процедурой обмена
	 *
	 */
	protected class WSSimpleExchangeObserver extends SimpleExchangeObserver {

		public WSSimpleExchangeObserver(FbaActivity activity, Handler handler) {
			super(activity, handler);
		}

		@Override
		public void onStart(ExchangeVariant variant) {
			super.onStart(variant);
			if (!mCanceled) {
				createProgressExchangeDlg();
				onProgress(FbaDBExchangeActivity.this
						.getString(R.string.fba_exchange_start));
			}
		}

		@Override
		public void onBuild() {
			super.onBuild();
			if (!mCanceled) {
				createProgressExchangeDlg();
				onProgress(FbaDBExchangeActivity.this
						.getString(R.string.fba_exchange_bild));
			}
		}

		@Override
		public void onStepInfo(final String msg) {
			super.onStepInfo(msg);
			Dbg.log(TAG + ".onStepInfo: " + msg);
			if (!mCanceled) {
				onProgress(msg);
			}
		}

		@Override
		public void onFinish(boolean success) {
			super.onFinish(success);
			closeProgressExchangeDlg();
		}

	}

}
