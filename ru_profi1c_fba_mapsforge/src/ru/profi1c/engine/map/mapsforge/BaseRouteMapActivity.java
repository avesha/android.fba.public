package ru.profi1c.engine.map.mapsforge;

import java.util.List;

import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.overlay.Overlay;
import org.mapsforge.android.maps.overlay.OverlayItem;
import org.mapsforge.core.model.GeoPoint;

import ru.profi1c.engine.map.mapsforge.MovableOverlay.MoveOverlayItemListener;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

/**
 * Базовый класс Mapsforge MapActivity с помощником добавления и навигации по
 * точкам маршрута, см {@link #addRouteOverlay(MapView, List, Drawable)}
 *
 */
public abstract class BaseRouteMapActivity extends BaseMapActivity implements
		OnTouchListener, MoveOverlayItemListener {

	public static final int NO_SELECTION = -1;

	private RouteOverlay<?> mRouteOverlay;
	private int mCurRouteItemIndex;
	private long mOnMoveTimeMs;

	/**
	 * Событие возникает при выборе точки маршрута. (интерактивно - по клику
	 * пользователем по нему или при программной навигации - см
	 * {@link #navigateRoute(int)})
	 *
	 * @param item
	 *            Выбранный элемент
	 */
	protected abstract void onRouteItemSelect(RouteOverlayItem<?> item);

	@Override
	protected void onCreateMapView(MapView mapView) {
		super.onCreateMapView(mapView);
		mapView.setOnTouchListener(this);
	}

	/**
	 * Добавить слой с точками маршрута на карту
	 *
	 * @param mapView
	 *            карта
	 * @param items
	 *            список точек маршрута
	 * @param defaultMarker
	 *            маркер по умолчанию
	 */
	public <T> void addRouteOverlay(MapView mapView,
			List<RouteOverlayItem<T>> items, Drawable defaultMarker) {

		RouteOverlay<T> routeOverlay = new RouteOverlay<T>(mapView,
				defaultMarker);
		routeOverlay.setMoveOverlayItemListener(this);

		for (RouteOverlayItem<T> routeItem : items) {
			routeOverlay.addItem(routeItem);
		}
		mapView.getOverlays().add(routeOverlay);

		mRouteOverlay = routeOverlay;
		mCurRouteItemIndex = NO_SELECTION;
	}

	/**
	 * Навигация по маршруту - позиционируемся на точке
	 *
	 * @param step
	 *            шаг, +1 - вперед на один элемент, -1 - назад на 1 элемент
	 */
	protected void navigateRoute(int step) {

		if (mRouteOverlay == null)
			throw new IllegalStateException(
					"The route is not added to the map, use 'addRouteOverlay' method!");

		int count = mRouteOverlay.getItems().size() - 1;
		int nextIndex = mCurRouteItemIndex + step;

		if (nextIndex > count)
			nextIndex = 0;
		else if (nextIndex < 0)
			nextIndex = count;

		setCenterRouteItem(nextIndex);
	}

	/**
	 * Установить точку маршрута по центру карты
	 *
	 * @param index
	 */
	protected void setCenterRouteItem(int index) {

		if (mRouteOverlay == null)
			throw new IllegalStateException(
					"The route is not added to the map, use 'addRouteOverlay' method!");

		GeoPoint p = mRouteOverlay.getItems().get(index).getGeoPoint();
		mRouteOverlay.getMapView().getMapViewPosition().setCenter(p);
		mCurRouteItemIndex = index;

		onRouteItemSelect(mRouteOverlay.getItems().get(index));
	}

	/**
	 * Индекс текущей точки маршрута
	 */
	protected int getCurrentRouteItemIndex() {
		return mCurRouteItemIndex;
	}

	/**
	 * Получить элемент маршрута по индексу
	 *
	 * @param index
	 * @return
	 */
	protected RouteOverlayItem<?> getRouteOverlayItem(int index) {

		if (mRouteOverlay == null)
			throw new IllegalStateException(
					"The route is not added to the map, use 'addRouteOverlay' method!");

		return mRouteOverlay.getItems().get(index);
	}

	/**
	 * Обновить отображение элементов маршрута
	 */
	protected void requestRedrawRouteOverlay() {
		if (mRouteOverlay == null)
			throw new IllegalStateException(
					"The route is not added to the map, use 'addRouteOverlay' method!");

		mRouteOverlay.getMapView().getOverlayController().redrawOverlays();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {

		if (v instanceof MapView) {
			MapView mapView = (MapView) v;

			for (Overlay overlay : mapView.getOverlays()) {
				if (overlay instanceof ITouchOverlay) {
					return ((ITouchOverlay) overlay).onTouchEvent(event,
							mapView);
				}
			}
		}
		return false;
	}

	@Override
	public void onMove(OverlayItem item) {

		if (item instanceof RouteOverlayItem) {
			RouteOverlayItem<?> routeItemOverlay = (RouteOverlayItem<?>) item;
			routeItemOverlay.setMoved(true);
		}
		mOnMoveTimeMs = System.currentTimeMillis();
	}

	@Override
	public void onTap(OverlayItem item, MapView mapView) {
		/*
		 * если между событиями маленький интервал, значит это автоматически
		 * сработало событие onTap по окончании перетаскивания элементов
		 */
		if (System.currentTimeMillis() - mOnMoveTimeMs > 500) {
			mCurRouteItemIndex = mRouteOverlay.getItems().lastIndexOf(item);
			onRouteItemSelect((RouteOverlayItem<?>) item);
		}

	}
}
