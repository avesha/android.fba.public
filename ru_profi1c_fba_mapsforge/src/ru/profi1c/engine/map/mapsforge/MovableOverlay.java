package ru.profi1c.engine.map.mapsforge;

import java.util.List;

import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.Projection;
import org.mapsforge.android.maps.overlay.ListOverlay;
import org.mapsforge.android.maps.overlay.OverlayItem;
import org.mapsforge.core.model.GeoPoint;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Vibrator;
import android.view.MotionEvent;

/**
 * Слой с элементами, которые можно двигать по карте
 *
 */
public abstract class MovableOverlay<Item extends MovableOverlayItem> extends
	ListOverlay implements ITouchOverlay {
	// private static final String TAG = "MovableOverlay";

	public interface MoveOverlayItemListener {
		public void onMove(OverlayItem item);
		public void onTap(OverlayItem item, MapView mapView);
	}

	private final MapView mapView;
	private final Drawable defaultMarker;

	private final Vibrator vibrator;
	private MoveOverlayItemListener moveOverlayListener;

	public MovableOverlay(MapView mapView, Drawable defaultMarker) {
		this.mapView = mapView;
		this.defaultMarker = defaultMarker;

		// для вибрации
		vibrator = (Vibrator) mapView.getContext().getSystemService(
				Context.VIBRATOR_SERVICE);

	}

	public MapView getMapView() {
		return mapView;
	}

	public void setMoveOverlayItemListener(MoveOverlayItemListener listener) {
		moveOverlayListener = listener;
	}

	public void addItem(Item item){
		getOverlayItems().add(item);
	}

	public int size() {
		return getOverlayItems().size();
	}

	@SuppressWarnings("unchecked")
	public List<Item> getItems(){
		return (List<Item>) getOverlayItems();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event, MapView mapView) {

		int action = event.getAction();
		if (action == MotionEvent.ACTION_DOWN) {

			if (findItemOnTap((int) event.getX(), (int) event.getY(),
					mapView.getProjection()))
				vibrator.vibrate(20);
			else
				movableItem = null;

		} else if (action == MotionEvent.ACTION_UP) {

			movableItem = null;
			if(moveOverlayListener!=null){
				Item item = getItemOnTap((int) event.getX(), (int) event.getY(),
					mapView.getProjection());
				if(item!=null)
					moveOverlayListener.onTap(item, mapView);

			}

		} else if (action == MotionEvent.ACTION_MOVE) {
			if (movableItem != null) {
				// изменить geo данные
				GeoPoint p = mapView.getProjection().fromPixels(
						(int) event.getX() - dx, (int) event.getY() + dy);
				movableItem.setGeoPoint(p);

				// сообщаем, что передвинули
				if (moveOverlayListener != null) {
					moveOverlayListener.onMove(movableItem);
				}

				mapView.getOverlayController().redrawOverlays();
				return true;
			}
		}
		return false;

	}

	private MovableOverlayItem movableItem;
	private Point p;
	private int dx;
	private int dy;
	private int wd2, h; // высота и половина ширины картинки

	private boolean findItemOnTap(int x, int y, Projection projection) {

		boolean capturedItem = false;

		for (Item item : getItems()) {
			if (item.isMovable()) {

				// текущие geo данные маркера конвертируем в экранные координаты
				p = mapView.getProjection().toPixels(item.getGeoPoint(), p);

				// размеры маркера
				Drawable marker = item.getDrawable();
				if (marker == null)
					marker = defaultMarker;

				h = marker.getIntrinsicHeight();
				wd2 = marker.getIntrinsicWidth() / 2;

				// если клик по маркеру
				if (x > p.x - wd2 && x < p.x + wd2 && y > p.y - h && y < p.y) {
					capturedItem = true;
					dx = x - p.x;
					dy = p.y - y;

					movableItem = item;

					break;
				}
			}
		}

		return capturedItem;
	}

	private Point p_;

	private Item getItemOnTap(int x, int y, Projection projection) {

		Item found = null;
		for (Item item : getItems()) {

			// текущие geo данные маркера конвертируем в экранные координаты
			p_ = mapView.getProjection().toPixels(item.getGeoPoint(), p_);

			// размеры маркера
			Drawable marker = item.getDrawable();
			if (marker == null)
				marker = defaultMarker;

			int h_ = marker.getIntrinsicHeight();
			int	wd2_ = marker.getIntrinsicWidth() / 2;

			// если клик по маркеру
			if (x > p_.x - wd2_ && x < p_.x + wd2_ && y > p_.y - h_ && y < p_.y) {
				found = item;
				break;
			}
		}

		return found;
	}


}
