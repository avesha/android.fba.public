package ru.profi1c.engine.map.mapsforge;

import java.io.File;

import org.mapsforge.android.maps.MapView;
import org.mapsforge.core.model.GeoPoint;
import org.mapsforge.map.reader.header.FileOpenResult;

import ru.profi1c.engine.map.location.PlatformSpecificImplementationFactory;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

/**
 * Базовый класс Mapsforge MapActivity с доступом к базе данных и отображением
 * текущей позиции
 *
 */
public abstract class BaseMapActivity extends FbaDBMapActivity {

	private CurrentPosOverlay mCurrentPosOverlay;

	private LocationManager mLocationManager;
	private LocationUpdateListener mLocationListener;

	/**
	 * Файл карты для off-line работы. О создании карт читайте {@link http
	 * ://code.google.com/p/mapsforge/wiki/GettingStartedMapWriter}. Карты
	 * некоторых городов (областей) России вы можете скачать с сайта
	 * www.profi1c.ru
	 */
	public abstract File getMapFile();

	/**
	 * Координаты центра карты по умолчанию
	 */
	public abstract GeoPoint getMapCenterPoint();

	/**
	 * Показывать маркер текущей позиции, будет создан автоматически при
	 * иницизации карты методом {@link #onCreateMapView(MapView mapView) }.
	 */
	public abstract boolean showCurrentPosition();

	/**
	 * Включить отслеживание изменения текущей позиции, при имении позиции
	 * вызывается {@link #onLocationChanged(Location location)}
	 */
	public abstract boolean requestLocationUpdates();

	/**
	 * Вызывается, когда положение изменилось (при условии, что включено
	 * отслеживание, см. {@link #requestLocationUpdates()}
	 *
	 * @param location
	 */
	public abstract void onLocationChanged(Location location);

	/**
	 * На сколько должна изменится текущая позиция (в метрах) прежде чем будет
	 * получено новое значение координат
	 */
	public abstract int getLocationMinDistance();

	/**
	 * Максимальное время, которое должно пройти, прежде чем пользователь
	 * получает обновление местоположения.
	 */
	public abstract long getLocationMinTime();

	/**
	 * Инициализация карты
	 *
	 * @param mapView
	 */
	protected void onCreateMapView(MapView mapView) {

		mapView.setClickable(true);
		mapView.setBuiltInZoomControls(true);

		if (loadMapFile(mapView)) {
			GeoPoint p = getMapCenterPoint();
			if (p != null)
				mapView.getMapViewPosition().setCenter(p);
			mapView.getMapViewPosition().setZoomLevel(MapHelper.ZOOM_LEVEL_AREA);

			if (showCurrentPosition()) {
				mCurrentPosOverlay = new CurrentPosOverlay(this,mapView);
				mCurrentPosOverlay.enableMyLocation(true);
				mapView.getOverlays().add(mCurrentPosOverlay);
			}
		} else {
			finish();
		}

	}

	/**
	 * Прочитать файл карты
	 *
	 * @param mapView
	 * @return
	 */
	protected boolean loadMapFile(MapView mapView) {

		boolean loaded = false;
		File fMap = getMapFile();
		if (fMap.exists()) {

			FileOpenResult fileOpenResult = mapView.setMapFile(getMapFile());
			if (fileOpenResult.isSuccess())
				loaded = true;
			else
				showToast(fileOpenResult.getErrorMessage());
		} else
			showToast(getString(R.string.fba_map_file_not_found));

		return loaded;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (requestLocationUpdates()) {
			initRequestLocationUpdates();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (mCurrentPosOverlay != null) {
			mCurrentPosOverlay.disableMyLocation();
		}

		remoteLinstenerLocationUpdate();
	}

	/*
	 * Инициализация слушателя изменения текущей позиции
	 */
	private void initRequestLocationUpdates() {

		mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		mLocationListener = new LocationUpdateListener();

		long minTime = getLocationMinTime();
		int minDistance = getLocationMinDistance();

		if (PlatformSpecificImplementationFactory.isProviderSupported(
				mLocationManager, LocationManager.NETWORK_PROVIDER))
			mLocationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER, minTime, minDistance,
					mLocationListener);

		if (PlatformSpecificImplementationFactory.isProviderSupported(
				mLocationManager, LocationManager.GPS_PROVIDER))
			mLocationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, minTime, minDistance,
					mLocationListener);
	}

	/*
	 * отключить слушатель изменения координат, если он есть
	 */
	private void remoteLinstenerLocationUpdate() {
		if (mLocationManager != null) {
			mLocationManager.removeUpdates(mLocationListener);
		}
	}

	/*
	 * Обработчик уведомления о получении текущей позиции
	 */
	private class LocationUpdateListener implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {
			BaseMapActivity.this.onLocationChanged(location);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {

		}

		@Override
		public void onProviderEnabled(String provider) {

		}

		@Override
		public void onProviderDisabled(String provider) {

		}

	}
}
