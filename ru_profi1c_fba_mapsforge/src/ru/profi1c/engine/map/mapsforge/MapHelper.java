package ru.profi1c.engine.map.mapsforge;

import org.mapsforge.core.model.GeoPoint;

import ru.profi1c.engine.util.MediaHelper;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;

public class MapHelper {

	public static final byte ZOOM_LEVEL_CITY = 13;
	public static final byte ZOOM_LEVEL_AREA = 14;
	public static final byte ZOOM_LEVEL_KVARTAL = 15;
	public static final byte ZOOM_LEVEL_STREET = 16;
	public static final byte ZOOM_LEVEL_MAX = 21;

	/**
	 * Представление (широта и долгота в градусах)
	 *
	 * @param p
	 * @return
	 */
	public static String formatGeoPoint(GeoPoint p) {
		return p.latitude + "," + p.longitude / 1E6;
	}

	/**
	 * преобразование в гео координаты.
	 *
	 * @param lat
	 *            Широта в градусах
	 * @param lon
	 *            Долгота в градусах
	 * @return
	 */
	@Deprecated
	public static GeoPoint toGeoPoint(double lat, double lon) {
		return (new GeoPoint(lat, lon));
	}

	/**
	 * Как географическое местоположение (время текущее).
	 *
	 * @param lat
	 *            Широта в градусах
	 * @param lng
	 *            Долгота в градусах
	 * @return
	 */
	public static Location getLocation(double lat, double lng) {

		Location loc = new Location(LocationManager.GPS_PROVIDER);
		loc.setLatitude(lat);
		loc.setLongitude(lng);
		loc.setTime(System.currentTimeMillis());

		return loc;

	}

	/**
	 * Создать пронумерованный маркер
	 *
	 * @param ctx
	 *            Текущий контекст
	 * @param resIdDrawable
	 *            Идентификатор ресурса маркера
	 * @param number
	 *            Число выводимое по центру маркера
	 * @return
	 */
	public static BitmapDrawable makeNumberedMarker(Context ctx,
			int resIdDrawable, int number) {
		return MediaHelper.drawTextOnDrawable(ctx, resIdDrawable,
				String.valueOf(number), 20);
	}

}
