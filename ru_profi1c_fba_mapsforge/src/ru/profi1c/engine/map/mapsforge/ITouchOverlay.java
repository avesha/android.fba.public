package ru.profi1c.engine.map.mapsforge;

import org.mapsforge.android.maps.MapView;

import android.view.MotionEvent;

public interface ITouchOverlay {
	public boolean onTouchEvent(MotionEvent event, MapView mapView);
}
