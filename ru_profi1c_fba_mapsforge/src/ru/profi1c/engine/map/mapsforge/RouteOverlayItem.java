package ru.profi1c.engine.map.mapsforge;

import org.mapsforge.core.model.GeoPoint;

import android.graphics.drawable.Drawable;

/**
 * Точка маршрута с дополнительными данными
 *
 * @param <T>
 */
public class RouteOverlayItem<T> extends MovableOverlayItem {

	private T mData;
	private boolean mMoved;
	private int mOrdinal;

	public RouteOverlayItem(GeoPoint geoPoint, Drawable drawable) {
		super(geoPoint, boundCenterBottom(drawable));
	}

	public RouteOverlayItem(GeoPoint geoPoint, Drawable drawable,T data) {
		super(geoPoint, boundCenterBottom(drawable));
		mData = data;
	}

	/**
	 * Возвращает данные связанные с этой точкой маршрута
	 */
	public T getData() {
		return mData;
	}

	/**
	 * Установить дополнительные данные
	 *
	 * @param data
	 */
	public void setData(T data) {
		this.mData = data;
	}

	/**
	 * Получить порядковый номер маршрута
	 */
	public int getOrdinal() {
		return mOrdinal;
	}

	/**
	 * Установить порядковый номер маршрута
	 */
	public void setOrdinal(int mOrdinal) {
		this.mOrdinal = mOrdinal;
	}

	/**
	 * Возвращает признак того, что элемент был перемещен на карте
	 */
	public boolean isMoved() {
		return mMoved;
	}

	/**
	 * Установить признак того, что элемент перемещался
	 *
	 * @param moved
	 */
	public void setMoved(boolean moved) {
		this.mMoved = moved;
	}
}
