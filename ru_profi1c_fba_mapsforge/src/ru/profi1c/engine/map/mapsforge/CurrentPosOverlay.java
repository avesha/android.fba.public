package ru.profi1c.engine.map.mapsforge;

import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.overlay.Marker;
import org.mapsforge.android.maps.overlay.MyLocationOverlay;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * Класс для отрисовки текущей позиции на карте
 * <code>mapsforge.android.maps.MapView</code>
 *
 */
public class CurrentPosOverlay extends MyLocationOverlay {

	public CurrentPosOverlay(Context context, MapView mapView, Drawable drawable) {
		super(context, mapView, drawable);
	}

	public CurrentPosOverlay(Context context, MapView mapView) {
		super(context, mapView, Marker.boundCenter(context.getResources().getDrawable(
				R.drawable.fba_map_indicator_current_position)));
	}

}
