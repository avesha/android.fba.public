package ru.profi1c.engine.map.mapsforge;

import org.mapsforge.android.maps.MapActivity;

import android.content.Intent;
import android.os.Bundle;

import com.j256.ormlite.support.ConnectionSource;

import ru.profi1c.engine.app.BaseAppSettings;
import ru.profi1c.engine.app.FbaActivity;
import ru.profi1c.engine.app.FbaActivityHelper;
import ru.profi1c.engine.app.FbaDBHelperWrapper;
import ru.profi1c.engine.exchange.BaseExchangeSettings;
import ru.profi1c.engine.meta.DBOpenHelper;
import ru.profi1c.engine.meta.MetadataHelper;

/**
 * Базовый класс для использования
 * <code>mapsforge.android.maps.MapActivity</code> c доступом к базе данных. Вы
 * можете просто вызвать {@link #getHelper()}, чтобы получить ваш класс
 * помощника, или {@link #getConnectionSource()}, чтобы получить
 * {@link ConnectionSource}.
 * 
 */
public abstract class FbaDBMapActivity extends MapActivity {

	final FbaActivityHelper mActivityHelper = FbaActivityHelper
			.createInstance(this);

	public FbaActivityHelper getActivityHelper() {
		return mActivityHelper;
	}

	private FbaDBHelperWrapper mDBHelperWrapper;

	/**
	 * Get a helper for this action.
	 */
	public DBOpenHelper getHelper() {
		if (mDBHelperWrapper == null) {
			throw new IllegalStateException(
					"DBOpenHelper was not available, because the event 'onCreate()' has not yet arrived!");
		}
		return mDBHelperWrapper.getHelper();
	}

	/**
	 * Get a connection source for this action.
	 */
	public ConnectionSource getConnectionSource() {
		if (mDBHelperWrapper == null) {
			throw new IllegalStateException(
					"DBOpenHelper was not available, because the event 'onCreate()' has not yet arrived!");
		}
		return mDBHelperWrapper.getConnectionSource();
	}

	/**
	 * Перейти к основной активности (Activity)
	 */
	public void goHome(FbaActivity activity) {
		mActivityHelper.goHome(activity);
	}

	/**
	 * Помощник доступа к намерению для запуска основной активности (Activity)
	 * 
	 * @return
	 */
	public Intent getHomeIntent() {
		return mActivityHelper.getHomeIntent();
	}

	/**
	 * Помощник для работы с метаданными объектов определенный для этого
	 * приложения fbaApplication.getMetadataHelper()
	 * 
	 * @return
	 */
	public MetadataHelper getMetadataHelper() {
		return mActivityHelper.getMetadataHelper();
	}

	/**
	 * Настройки обмена web-сервисом 1С определенные для этого приложения
	 * fbaApplication.getExchangeSettings()
	 * 
	 * @return
	 */
	public BaseExchangeSettings getExchangeSettings() {
		return mActivityHelper.getExchangeSettings();
	}

	/**
	 * Настройки приложения определенные для этого приложения
	 * fbaApplication.getAppSettings()
	 * 
	 * @return
	 */
	public BaseAppSettings getAppSettings() {
		return mActivityHelper.getAppSettings();
	}

	/**
	 * Возвращает истина, если текущая активити является главной
	 * 
	 * @param activity
	 * @return
	 */
	public boolean isMainActivity() {
		return mActivityHelper.isMainActivity();
	}

	/**
	 * Открыть настройки программы. Активити, которая используется для
	 * редактирования настроек задается в App.getPreferenceActivityClass()
	 */
	public void showPreferenceActivity() {
		mActivityHelper.showPreferenceActivity();
	}

	/**
	 * Открыть диалог авторизации пользователя при запуске программы
	 * 
	 * @param requestCode
	 */
	public void showLoginActivty(int requestCode) {
		mActivityHelper.showLoginActivty(requestCode);
	}

	/**
	 * Показать всплывающее уведомление
	 * 
	 * @param message
	 *            сообщение
	 */
	public void showToast(final String message) {
		mActivityHelper.showToast(message);
	}

	/**
	 * Показать простой диалог c сообщением и кнопкой ОК
	 * 
	 * @param title
	 *            заголовок диалога
	 * @param message
	 *            сообщение
	 */
	public void showMessage(String title, String message) {
		mActivityHelper.showMessage(title, message);
	}

	/**
	 * Показать простой диалог c сообщением и кнопкой ОК
	 * 
	 * @param resIdTitle
	 *            идентификатор строкового ресурса для заголовка диалога
	 * @param message
	 *            сообщение
	 */
	public void showMessage(int resIdTitle, String message) {
		mActivityHelper.showMessage(this.getString(resIdTitle), message);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mDBHelperWrapper = new FbaDBHelperWrapper(getApplicationContext());
		mDBHelperWrapper.onCreate();

		mActivityHelper.onCreate(savedInstanceState);

		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mActivityHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mDBHelperWrapper.onDestroy();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "@"
				+ Integer.toHexString(super.hashCode());
	}
}
