package ru.profi1c.engine.map.mapsforge;

import org.mapsforge.android.maps.overlay.Marker;
import org.mapsforge.core.model.GeoPoint;

import android.graphics.drawable.Drawable;

/**
 * Элемент, который хранит свойство что его можно двигать
 *
 */
public class MovableOverlayItem extends Marker {

	/**
	 * Краткое описание элемента.
	 */
	protected String snippet;

	/**
	 * Название этого пункта.
	 */
	protected String title;

	protected boolean movable;

	public MovableOverlayItem(GeoPoint geoPoint, Drawable drawable) {
		super(geoPoint, drawable);
	}


	public synchronized boolean isMovable() {
		return movable;
	}

	public synchronized void setMovable(boolean movable) {
		this.movable = movable;
	}

	/**
	 * @return краткое описание этого пункта (может быть пустым).
	 */
	public synchronized String getSnippet() {
		return this.snippet;
	}

	/**
	 * @return название данного пункта, (может быть пустым).
	 */
	public synchronized String getTitle() {
		return this.title;
	}

	/**
	 * Задает краткое описание этого пункта.
	 *
	 * @param snippet
	 *            краткое описание элемента (может быть пустым).
	 */
	public synchronized void setSnippet(String snippet) {
		this.snippet = snippet;
	}

	/**
	 * Задает название данного пункта.
	 *
	 * @param title
	 *            Название элемента (может быть пустым).
	 */
	public synchronized void setTitle(String title) {
		this.title = title;
	}
}
