package ru.profi1c.samples.order;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import ru.profi1c.engine.app.SimpleCatalogListActivity;
import ru.profi1c.engine.meta.RowDao;
import ru.profi1c.samples.order.db.CatalogNomenklatura;
import ru.profi1c.samples.order.db.CatalogNomenklaturaDao;
import android.os.Bundle;
import android.widget.Filterable;
import android.widget.ListAdapter;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnActionExpandListener;

/*
 * Форма списка справочника «Номенклатура»
 */
public class CatalogNomenklaturaListActivity extends
		SimpleCatalogListActivity<CatalogNomenklatura> {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.fba_include_simple_list_layout);
		try {
			setContentListView();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.activity_nomenklatura_list, menu);

		//Кнопка поиска в залоговке
		MenuItem item = menu.findItem(R.id.menu_search);
		SearchView searchView = (SearchView) item.getActionView();
		searchView.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String query) {
				filterData(query);
				return true;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				return true;
			}
		});
		item.setOnActionExpandListener(new OnActionExpandListener() {

			@Override
			public boolean onMenuItemActionExpand(MenuItem item) {
				return true;
			}

			@Override
			public boolean onMenuItemActionCollapse(MenuItem item) {
				filterData(null);
				return true;
			}
		});
		//item.se

		return true;
	}

	@Override
	protected List<CatalogNomenklatura> select(RowDao<CatalogNomenklatura> dao)
			throws SQLException {

		/*
		 * Выбрать все записи (не установлен отбор первым параметром)
		 * отсортированные по наименованию
		 */
		return dao.select(null, CatalogNomenklatura.FIELD_NAME_DESCRIPTION);
	}

	/*
	 * Установит отбор данных (по частичному совпадению представления)
	 */
	protected void filterData(String query) {

		ListAdapter adapter = getListAdapter();
		if (adapter instanceof Filterable) {
			Filterable filterable = (Filterable) adapter;
			filterable.getFilter().filter(query);
		}
	}

}
