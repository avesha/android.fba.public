/*
 * This text is generated automatically, do not delete it. 'Android App Builder', www.profi1c.ru
 */
package ru.profi1c.samples.tracker;

import ru.profi1c.engine.app.FbaActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

/*
 * Эта Activity является основной и будет первой отображаться при запуске
 * приложения.
 *
 * Предупреждение! FBA использует библиотеку 'ActionBarSherlock' http://actionbarsherlock.com
 * Вы должны включить эти директивы импорта:
 * import com.actionbarsherlock.app.ActionBar;
 * import com.actionbarsherlock.view.Menu;
 * import com.actionbarsherlock.view.MenuItem;
 * вместо стандартных.
 *
 * Взаимодействие с панелью действий обрабатывается путем вызова
 * getSupportActionBar() (вместо getActionBar()).
 *
 * Для того чтобы создать ваши  меню из XML следует вызывать
 * getSupportMenuInflater() в вашей деятельности (вместо getMenuInflater()).
 *
 * @author ООО "Сфера" (info@sfera.ru)
 *
 */
public class MainActivity extends FbaActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		init();
	}

	private void init() {
		TextView tvStatus = (TextView) findViewById(R.id.status);
		tvStatus.setText("Текущий статус: "
				+ (LocationService.isRunning() ? "Включен" : "Выключен"));

		Button bnt = (Button) findViewById(R.id.btnStart);
		bnt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				LocationService.start(MainActivity.this,
						Const.LOCATION_MIN_TIME, Const.LOCATION_MIN_DISTANCE);
			}
		});

		bnt = (Button) findViewById(R.id.btnStop);
		bnt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				LocationService.stop(MainActivity.this);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.fba_menu_settings) {

			showPreferenceActivity();
			return true;

		}
		return super.onOptionsItemSelected(item);
	}


}