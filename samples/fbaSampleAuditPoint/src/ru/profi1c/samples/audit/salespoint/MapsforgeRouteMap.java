package ru.profi1c.samples.audit.salespoint;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.overlay.Marker;
import org.mapsforge.core.model.GeoPoint;

import ru.profi1c.engine.map.mapsforge.BaseRouteMapActivity;
import ru.profi1c.engine.map.mapsforge.MapHelper;
import ru.profi1c.engine.map.mapsforge.RouteOverlayItem;
import ru.profi1c.engine.widget.ObjectView;
import ru.profi1c.samples.audit.salespoint.db.CatalogAddInfoStorage;
import ru.profi1c.samples.audit.salespoint.db.CatalogAddInfoStorageDao;
import ru.profi1c.samples.audit.salespoint.db.CatalogSalesPoint;
import ru.profi1c.samples.audit.salespoint.db.CatalogSalesPointDao;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

/*
 * Пример отображение маршрута на оффлайн карте
 */
public class MapsforgeRouteMap extends BaseRouteMapActivity {
	private static final String TAG = "MapsforgeRouteMap";

	/*
	 * На сколько должна изменится текущая позиция (в метрах) прежде чем будет
	 * получено новое значение координат
	 */
	public static int DEF_LOCATION_MIN_DISTANCE = 500;

	/*
	 * Максимальное время, которое должно пройти, прежде чем пользователь
	 * получает обновление местоположения.
	 */
	public static long DEF_LOCATION_MIN_TIME = 5000 * 60;

	//Москва, нулевой километр
	private static final double DEFAULT_GEOPOINT_LAT = 55.755831;
	private static final double DEFAULT_GEOPOINT_LNG = 37.617673;

	//Файл карты
	//брать здесь http://profi1c.ru/fba-toolkit-maps-mapsforge
	private static final String MAP_FILE = "ru_moscow.map";

	private MapView mMapView;

	private ObjectView mSalesPointView, mFotoStorageView;
	private CatalogAddInfoStorageDao mStorageDao;
	private CatalogSalesPointDao mSalesPointDao;
	private Animation mAnimationIn, mAnimationOut;

	// Список точек маршрута отображаемый на карте
	List<RouteOverlayItem<CatalogSalesPoint>> routeItems;

	/*
	 * Имена реквизитов отображаемых в всплывающем окне  (имена полей класса)
	 */
	private static String[] fields = new String[] {
		CatalogSalesPoint.FIELD_NAME_DESCRIPTION,
		CatalogSalesPoint.FIELD_NAME_ADRESS,
		CatalogSalesPoint.FIELD_NAME_PHONE,
		CatalogSalesPoint.FIELD_NAME_SITE
	};

	/*
	 * Идентификаторы view-элементов для отображения реквизитов
	 */
	private static int[] ids = new int[] { R.id.tvDescription, R.id.tvAdress,
			R.id.tvPhone, R.id.tvSite};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_route_map);
		init();
	}

	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		getMenuInflater().inflate(R.menu.activity_map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.menu_save_coordinates) {

			saveMovedSalesPoint();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void init() {
		mMapView = (MapView) findViewById(R.id.mapView);

		//Инициализация карты
		onCreateMapView(mMapView);

		mSalesPointView = (ObjectView) findViewById(R.id.ovSalesPoint);
		mSalesPointView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				closePopup();
			}
		});

		mFotoStorageView = (ObjectView) findViewById(R.id.ovFotoStorage);
		mAnimationIn = AnimationUtils.loadAnimation(this, R.anim.toast_enter);
		mAnimationOut = AnimationUtils.loadAnimation(this, R.anim.toast_exit);

		try {
			//торговые точки расположить на карте
			initDummyRoute();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Button bnt = (Button) findViewById(R.id.btnPrev);
		bnt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				navigateRoute(-1);
			}
		});
		bnt = (Button) findViewById(R.id.btnNext);
		bnt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				navigateRoute(1);
			}
		});

	}

	private void initDummyRoute() throws SQLException {

		//DAO для отображения картинки и для записи координат
		mStorageDao = new CatalogAddInfoStorageDao(getConnectionSource());
		mSalesPointDao = new CatalogSalesPointDao(getConnectionSource());

		// Выбрать все торговые точки
		CatalogSalesPointDao salePointDao = new CatalogSalesPointDao(getConnectionSource());
		List<CatalogSalesPoint> lst = salePointDao.select();

		// Список точек маршрута отображаемый на карте
		routeItems = new ArrayList<RouteOverlayItem<CatalogSalesPoint>>();

		// Для точек, у которых не установлены координаты
		final double lng = DEFAULT_GEOPOINT_LNG;
		double lat = DEFAULT_GEOPOINT_LAT;

		int count = lst.size();
		for (int i = 0; i < count; i++) {

			CatalogSalesPoint salesPoint = lst.get(i);

			boolean movable = (salesPoint.lat == 0 || salesPoint.lng == 0);
			int resIdDrawable = R.drawable.fba_map_maker_green;
			GeoPoint geoPoint = new GeoPoint(salesPoint.lat, salesPoint.lng);

			if (movable) {
				resIdDrawable = R.drawable.fba_map_marker_red;
				lat -= 0.003f;
				geoPoint = new GeoPoint(lat, lng);
			}

			//На маркере вывести порядковый  номер
			Drawable marker = MapHelper.makeNumberedMarker(this, resIdDrawable,i + 1);

			RouteOverlayItem<CatalogSalesPoint> routePoint = new RouteOverlayItem<CatalogSalesPoint>(
					geoPoint, marker, salesPoint);
			routePoint.setMovable(movable);
			routePoint.setOrdinal(i + 1);

			routeItems.add(routePoint);
		}

		Drawable defaultMarker = getResources().getDrawable(R.drawable.fba_map_marker_orange);
		addRouteOverlay(mMapView, routeItems, defaultMarker);

	}

	@Override
	public File getMapFile() {
		return new File(getAppSettings().getBackupDir(),MAP_FILE);
	}

	@Override
	public GeoPoint getMapCenterPoint() {
		return new GeoPoint(DEFAULT_GEOPOINT_LAT, DEFAULT_GEOPOINT_LNG);
	}

	@Override
	public int getLocationMinDistance() {
		return DEF_LOCATION_MIN_DISTANCE;
	}

	@Override
	public long getLocationMinTime() {
		return DEF_LOCATION_MIN_TIME;
	}

	@Override
	public boolean showCurrentPosition() {
		return true;
	}

	@Override
	public boolean requestLocationUpdates() {
		return true;
	}

	@Override
	protected void onRouteItemSelect(RouteOverlayItem<?> item) {

		try {
			CatalogSalesPoint salesPoint = (CatalogSalesPoint) item.getData();
			inflatePopup(salesPoint);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		Log.d(TAG, "new location: lat = " + location.getLatitude() + ", lng = "
				+ location.getLongitude());
	}

	/*
	 * Заполнить данные по торговой точке на всплывающем окне и отобразить его
	 */
	private void inflatePopup(CatalogSalesPoint salesPoint) throws SQLException {
		closePopup();

		if (CatalogSalesPoint.isEmpty(salesPoint.foto)) {
			mFotoStorageView.setVisibility(View.GONE);
		} else {
			//Прочитать ссылку на фото
			mStorageDao.refresh(salesPoint.foto);
			mFotoStorageView.build(salesPoint.foto, getHelper(),
					new String[] { CatalogAddInfoStorage.FIELD_NAME_STORAGE },
					new int[] { R.id.ivFoto });

			mFotoStorageView.setVisibility(View.VISIBLE);
		}

		mSalesPointView.build(salesPoint, getHelper(), fields, ids);
		showPopup();
	}

	/*
	 * Скрыть всплывающее окно
	 */
	private void closePopup(){
		if(mSalesPointView.getVisibility() ==  View.VISIBLE){
			mSalesPointView.startAnimation(mAnimationOut);
			mSalesPointView.setVisibility(View.GONE);
		}
	}

	/*
	 * Показать всплывающее окно
	 */
	private void showPopup(){
		if(mSalesPointView.getVisibility() !=  View.VISIBLE){
			mSalesPointView.startAnimation(mAnimationIn);
			mSalesPointView.setVisibility(View.VISIBLE);
		}
	}

	/*
	 * Сохранить координаты точек, которые пользователь установил на карте
	 * вручную
	 */
	private void saveMovedSalesPoint() {

		int count = 0;
		for (RouteOverlayItem<CatalogSalesPoint> routeItem : routeItems) {
			if (routeItem.isMoved()) {

				// Обновим и сохраним в локальную базу (с обменом будет передано
				// в 1С)
				CatalogSalesPoint salesPoint = routeItem.getData();
				salesPoint.lat = routeItem.getGeoPoint().latitude;
				salesPoint.lng = routeItem.getGeoPoint().longitude;
				salesPoint.setModified(true);

				try {
					mSalesPointDao.update(salesPoint);
					count++;
				} catch (SQLException e) {
					e.printStackTrace();
				}

				Drawable marker = MapHelper.makeNumberedMarker(this, R.drawable.fba_map_maker_green,routeItem.getOrdinal());

				// Маркер больше не двигаем
				routeItem.setDrawable(Marker.boundCenterBottom(marker));
				routeItem.setMovable(false);
			}
		}

		if (count > 0) {
			requestRedrawRouteOverlay();
			showToast(getString(R.string.msg_save_point_coodinates));
		}

	}
}
