/*
 * This text is generated automatically, do not delete it. 'Android App Builder', www.profi1c.ru
 */
package ru.profi1c.samples.audit.salespoint;

import ru.profi1c.engine.app.FbaDBExchangeActivity;
import ru.profi1c.engine.exchange.ExchangeObserver;
import ru.profi1c.engine.exchange.ExchangeReceiver;
import ru.profi1c.engine.exchange.ExchangeVariant;
import ru.profi1c.engine.util.AppHelper;
import ru.profi1c.engine.widget.PresentationAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

/*
 * Эта Activity является основной и будет первой отображаться при запуске
 * приложения.
 *
 * Предупреждение! FBA использует библиотеку 'ActionBarSherlock' http://actionbarsherlock.com
 * Вы должны включить эти директивы импорта:
 * import com.actionbarsherlock.app.ActionBar;
 * import com.actionbarsherlock.view.Menu;
 * import com.actionbarsherlock.view.MenuItem;
 * вместо стандартных.
 *
 * Взаимодействие с панелью действий обрабатывается путем вызова
 * getSupportActionBar() (вместо getActionBar()).
 *
 * Для того чтобы создать ваши  меню из XML следует вызывать
 * getSupportMenuInflater() в вашей деятельности (вместо getMenuInflater()).
 *
 * @author ООО "Сфера" (support@sfera.ru)
 *
 */
public class MainActivity extends FbaDBExchangeActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if(savedInstanceState==null && isMainActivity()){
			onCreateNewSession();
		}

		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.fba_menu_settings) {

			showPreferenceActivity();
			return true;

		} else if (id == R.id.fba_menu_exchange) {

			doSelectStartExchange();
			return true;

		} else if (id == R.id.menu_map) {

			startActivity(new Intent(this, MapsforgeRouteMap.class));

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected ExchangeObserver getExchangeObserver() {
		return null;
	}

	/**
	 * Запущена новая сессия приложения т.н запуск, не поворот экрана
	 */
	private void onCreateNewSession() {

		Context ctx = getApplicationContext();

		//восстановить задание обмена в планировщике если требуется
		if(AppHelper.isAppInstalledToSDCard(ctx)) {
			ExchangeReceiver.createSchedulerTasks(ctx);
		}

	}

	/*
	 * Интерактивный выбор варианта и запуск обмена
	 */
	private void doSelectStartExchange() {

		// адаптер для отображения значений перечислений в диалоге выбора
		PresentationAdapter adapter = new PresentationAdapter(this,
				android.R.layout.simple_spinner_dropdown_item,
				ExchangeVariant.values());

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				ExchangeVariant variant = ExchangeVariant.values()[which];
				startExchange(variant, true);
				dialog.dismiss();

			}
		});
		builder.setTitle("Выбор варианта обмена");
		builder.create().show();

	}

}