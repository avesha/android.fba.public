/*
 * This text is generated automatically, do not delete it. 'Android App Builder', www.profi1c.ru
 */
package ru.profi1c.samples.report.head.exchange;

import java.util.concurrent.FutureTask;

import ru.profi1c.engine.exchange.BaseExchangeManager;
import ru.profi1c.engine.exchange.BaseExchangeSettings;
import ru.profi1c.engine.exchange.ExchangeTask;
import ru.profi1c.engine.exchange.ExchangeVariant;
import ru.profi1c.engine.exchange.IExchangeCallbackListener;
import ru.profi1c.engine.meta.DBOpenHelper;

import ru.profi1c.samples.report.head.db.DBHelper;

/**
 * Менеджер обмена с web-сервисом
 * @author ООО "Сфера" (support@sfera.ru)
 *
 */
public class ExchangeManager extends BaseExchangeManager {

	public ExchangeManager(BaseExchangeSettings exchangeSettings) {
		super(exchangeSettings);
	}

	@Override
	protected DBOpenHelper getDBOpenHelper() {
		return new DBHelper(getContext());
	}
	
	/**
	 * Запуск процедуры обмена в рабочем процессе.
	 * Процедура обмена выполняется по фиксированным правилам определенным в  {@link ExchangeTask}
	 * @param exchangeVariant вариант обмена
	 * @return
	 */
	public boolean startExchange(ExchangeVariant exchangeVariant) throws Exception{
		
		if(!isRunningExchageTask()){
			ExchangeTask task = new ExchangeTask(exchangeVariant, getWSHelper(), getDBOpenHelper());
			return task.call();
		}
		return false;
	
	}
	
	/**
	 * Запуск процедуры обмена в отдельном потоке.
	 * Процедура обмена выполняется по фиксированным правилам определенным в  {@link ExchangeTask}
	 * @param exchangeVariant вариант обмена
	 * @param callbackListener обработчик обратного вызова, слушатель результата обмена
	 */
	public void startExchange(final ExchangeVariant exchangeVariant,final IExchangeCallbackListener listener){
		
		if(!isRunningExchageTask()){
		
			ExchangeTask task = new ExchangeTask(exchangeVariant, getWSHelper(), getDBOpenHelper());
			task.setListener(listener);
			
			FutureTask<Boolean> futureTask = new FutureTask<Boolean>(task);
			Thread thread = new Thread(futureTask);
			thread.start();
			
		}
				
	}

}

